﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSetup : MonoBehaviour {
    public static GameSetup singleton;
    public Transform[] playerSpawnPoints;
    public Transform[] enemySpawnPoints;
    private void OnEnable() {
        if (GameSetup.singleton == null) {
            GameSetup.singleton = this;
        }
    }
}
