﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class testingRaycastDistance : MonoBehaviour {
    [SerializeField] private GameObject startGo;
    [SerializeField] private GameObject endGo;
    [SerializeField] private GameObject pointDisplay;
    [SerializeField] private GameObject pointDisplay2;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Debug.DrawLine(startGo.transform.position, startGo.transform.TransformDirection(Vector3.forward) * 100, Color.red);
        RaycastHit hit;
        RaycastHit anotherHit;

        Ray ray = new Ray(startGo.transform.position, endGo.transform.position - startGo.transform.position);

        if (Physics.Raycast(ray, out hit)) {
            Debug.DrawLine( hit.point, startGo.transform.position, Color.green);
            pointDisplay.transform.position = hit.point;

            Ray ray2 = new Ray(hit.point, startGo.transform.position - hit.point);

            if (Physics.Raycast(ray2, out anotherHit)) {
                Debug.DrawLine( hit.point, anotherHit.point, Color.red);
                Debug.Log("Distance: " + anotherHit.distance);
                pointDisplay2.transform.position = anotherHit.point;
            }

        }


    }
}
