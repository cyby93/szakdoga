﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
[RequireComponent(typeof(Button))]
public class CharacterAbilityIcon : MonoBehaviour {
    private Unit localPlayerUnit;
    private PhotonNetworkPlayer _photonNetworkPlayer;
    private Image image;

    private Ability ability;
    private Button button;
    [SerializeField] private SlotKey hotkey;

    private void Awake() {
        image = GetComponent<Image>();
        button = GetComponent<Button>();
    }
    void Update() {
        if (!_photonNetworkPlayer && isLocalPlayerObjectExists()) {
            _photonNetworkPlayer = GameObject.Find("LocalPlayerObject").GetComponent<PhotonNetworkPlayer>();
        }
        if (_photonNetworkPlayer && isPlayerUnitExists() && !hasLocalPlayerUnitData()) {
            SetLocalPlayerUnit();
        }
        if (hasLocalPlayerUnitData()) {
            EnableButton();
        } 
    }
    public void useAbility() {
        localPlayerUnit.GetAbilityCastManager().SelectAbility(ability.hotkey);
    }
    private void SetLocalPlayerUnit() {
        localPlayerUnit = _photonNetworkPlayer.GetMyPlayerUnit().GetComponent<Unit>();
    }
    private bool hasLocalPlayerUnitData() {
        return localPlayerUnit != null;
    }
    private bool isLocalPlayerObjectExists() {
        return GameObject.Find("LocalPlayerObject");
    }
    private bool isPlayerUnitExists() {
        return _photonNetworkPlayer.GetMyPlayerUnit();
    }
    private void DisableButton() {
        image.enabled = false;
        button.enabled = false;
    }
    private void EnableButton() {
        image.enabled = true;
        image.color = Color.white;
        button.enabled = true;
        ability = SpellBookUtility.GetAbility(localPlayerUnit.spellBook, hotkey);
        image.sprite = ability.icon ? ability.icon : null;
    }
}