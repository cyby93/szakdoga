﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class InformationHeader : MonoBehaviourPunCallbacks {
    [SerializeField] private Text currentWaveText;
    [SerializeField] private Text timeUntilNextWave;
    [SerializeField] private Text goldText;

    private int maxWave;
    private float timeBetweenWaves;
    private int currentGold;
    private void Start() {
        maxWave = 0;
        currentGold = 0;
    }

    public void updateGoldText(int gold)
    {
        currentGold += gold;
        goldText.text = currentGold.ToString();
    }
    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged) {
        base.OnRoomPropertiesUpdate(propertiesThatChanged);
        if (propertiesThatChanged.ContainsKey(Constants.RoomProperty.MaxWaveNumber)) {
            maxWave = (int) propertiesThatChanged[Constants.RoomProperty.MaxWaveNumber];
            timeBetweenWaves = (float) propertiesThatChanged[Constants.RoomProperty.TimeBetweenWaves];
            NextWaveText((int) propertiesThatChanged[Constants.RoomProperty.CurrentWaveNumber]);
        }
        if (propertiesThatChanged.ContainsKey(Constants.RoomProperty.CurrentWaveNumber)) {
            UpdateText((int)propertiesThatChanged[Constants.RoomProperty.CurrentWaveNumber]);
            StartCountDownToNextWave();
        };
    }

    private void UpdateText(int currentWave) {
        currentWaveText.text = NextWaveText(currentWave);
    }

    private void StartCountDownToNextWave() {
        StartCoroutine(UpdateTimeLeftUntilNextWave());
    }

    private IEnumerator UpdateTimeLeftUntilNextWave() {
        int time = (int)timeBetweenWaves;
        while (time > 0) {
            timeUntilNextWave.text = TimeLeftText(time);
            time--;
            yield return new WaitForSeconds(1f);
        }

        timeUntilNextWave.text = "-";
    }
    private string NextWaveText(int currentWave) {
        return "Wave " + currentWave + "/" + maxWave;
    }

    private string TimeLeftText(int timeLeft) {
        return timeLeft + (timeLeft > 1 ? " secs" : " sec");
    }
}