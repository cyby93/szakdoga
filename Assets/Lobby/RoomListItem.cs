﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RoomListItem : MonoBehaviour {
    [SerializeField] private Text roomNameText;
    
    public string roomName;
    public int roomSize;
    public void SetRoom() {
        roomNameText.text = roomName + " (" + roomSize.ToString() + ")";
    }
    
    public void JoinRoom() {
        PhotonNetwork.JoinRoom(roomName);
        PhotonNetwork.LoadLevel(1);
    }
}