﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateCharacterTitle : MonoBehaviour
{
    [SerializeField]
    private Text Type;
    public void UpdateTitle(string _type){
        Type.text = _type;
    }
}
