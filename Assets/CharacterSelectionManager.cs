﻿using System;
using System.Collections;
using System.IO;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class CharacterSelectionManager : MonoBehaviourPunCallbacks, IInRoomCallbacks {
    public static CharacterSelectionManager singleton;


    public GameObject startButton;
    public GameObject readyButton;
    public GameObject loadingScreen;
    public GameObject countdownScreen;
    public Transform partyStatusContainer;
    public GameObject playerStatusPrefab;

    public override void OnEnable() {
        base.OnEnable();
        PhotonNetwork.AddCallbackTarget(this);
        SceneManager.sceneLoaded += OnSceneFinishedLoading;
    }

    public override void OnDisable() {
        base.OnDisable();
        PhotonNetwork.RemoveCallbackTarget(this);
        SceneManager.sceneLoaded -= OnSceneFinishedLoading;
    }

    void OnSceneFinishedLoading(Scene scene, LoadSceneMode mode) {
    }

    public override void OnJoinedRoom() {
        base.OnJoinedRoom();
        if (!PhotonNetwork.IsMasterClient) {
            startButton.SetActive(false);
        }

        CreatePlayer();
        RefreshPartyList();
        loadingScreen.SetActive(false);
    }
    
    private void CreatePlayer() {
        PhotonNetwork.Instantiate(Path.Combine("Prefabs", "PhotonNetworkPlayer"), transform.position,
                                  Quaternion.identity, 0);
        PhotonNetwork.LocalPlayer.CustomProperties = InitialHashTable();
    }

    private Hashtable InitialHashTable() {
        return new Hashtable() {{Constants.PlayerProperty.IsReady, false}};
    }


    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps) {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        RefreshPartyList();
        CheckIfEveryOneIsReadyToStart();
    }

    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged) {
        base.OnRoomPropertiesUpdate(propertiesThatChanged);
        if (propertiesThatChanged.ContainsKey(Constants.RoomProperty.IsCountdownOn) && (bool)propertiesThatChanged[Constants.RoomProperty.IsCountdownOn]) {
            countdownScreen.SetActive(true);
        };
        if (propertiesThatChanged.ContainsKey(Constants.RoomProperty.GameStarted) && (bool) propertiesThatChanged[Constants.RoomProperty.GameStarted]) {
            PhotonNetwork.LoadLevel(2);
        };
    }

    public void StartCountdown() {
        PhotonNetwork.CurrentRoom.SetCustomProperties(new Hashtable() {{Constants.RoomProperty.IsCountdownOn, true}});
    }

    private void RefreshPartyList() {
        ClearPartyItems();
        if (PhotonNetwork.InRoom) {
            foreach (Player player in PhotonNetwork.PlayerList) {
                GameObject tempListing = Instantiate(playerStatusPrefab, partyStatusContainer);
                PlayerPartyStatusConfig config = tempListing.GetComponent<PlayerPartyStatusConfig>();
                if (config) {
                    config.Setup(player);
                }
            }
        }
    }

    private void CheckIfEveryOneIsReadyToStart() {
        if (IsEveryPlayerReady() && PhotonNetwork.IsMasterClient) {
            startButton.SetActive(true);
        }
        else {
            startButton.SetActive(false);
        }
    }

    private bool IsEveryPlayerReady() {
        foreach (Player player in PhotonNetwork.PlayerList) {
            bool isReady = player.CustomProperties.ContainsKey(Constants.PlayerProperty.IsReady) && (bool) player.CustomProperties[Constants.PlayerProperty.IsReady];
            if (!isReady)
                return false;
        }

        return true;
    }
    public override void OnPlayerEnteredRoom(Player newPlayer) {
        base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log("New player entered!");
        RefreshPartyList();
        CheckIfEveryOneIsReadyToStart();
    }

    public override void OnPlayerLeftRoom(Player otherPlayer) {
        base.OnPlayerLeftRoom(otherPlayer);
        Debug.Log("Player left room: " + otherPlayer.NickName);
        RefreshPartyList();
        CheckIfEveryOneIsReadyToStart();
    }

    private void ClearPartyItems() {
        for (int i = partyStatusContainer.childCount - 1; i >= 0; i--) {
            Destroy(partyStatusContainer.GetChild(i).gameObject);
        }
    }

    public void ToggleCharacterReadyStatus() {
        bool isReady = (bool) PhotonNetwork.LocalPlayer.CustomProperties[Constants.PlayerProperty.IsReady];
        Hashtable newHash = new Hashtable() {{Constants.PlayerProperty.IsReady, !isReady}};
        PhotonNetwork.LocalPlayer.SetCustomProperties(newHash);
    }
}