﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Instatiaters {
public class UnitModelInstantiater
{
    public static GameObject InstatiateModel(GameObject _parent, GameObject _model)
    {
        GameObject instantiatedUnitModel = GameObject.Instantiate(_model) as GameObject;
        Transform modelTransform = instantiatedUnitModel.GetComponent<Transform>();
        modelTransform.SetParent(_parent.GetComponent<Transform>());
        modelTransform.localRotation = Quaternion.Euler(new Vector3(0,90,0));
        modelTransform.localPosition = Vector3.zero;
        return instantiatedUnitModel;
    }
}
}
