﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


namespace UnitDependencies
{
    static class Constants {
        public const float runningSpeed = 12;
        public const float walkingSpeed = 6;
        public static List<KeyValuePair<int, float>> experienceIntervallums = new List<KeyValuePair<int, float>>();
    }
    public struct AttributeST {
        public int strength;
        public int agility;
        public int intellect;
        public int stamina;
        public int spirit;
    }; 
    public struct Attributes {
        public int strength;
        public int agility;
        public int intellect;
        public int stamina;
        public int spirit;
        public float armor;
        public float movementSpeed;
        public float attackSpeed;
        public float attackRange;
        public float attackDamage;
        public float minDamage, maxDamage;
        public float ciritcalChance;
        public float spellPower;

    }; 
    public struct SpecialAttributesST {
        public float movementSpeed;
        public float attackSpeed;
        public float attackRange;
        public float healthRegeneration;
        public float energyRegeneration;
        public float attackPower;
        public float spellPower;
    };

    public enum CLASS {
        WARRIOR,
        PALADIN,
        SHAMAN,
        HUNTER,
        DRUID,
        ROGUE,
        MAGE,
        WARLOCK,
        PRIEST
    };

    public enum Race {
        HeroRace,
        UnitRace
    }
    public enum HeroRace {
        ORC,
        TROLL,
        TAUREN,
        UNDEAD,
        HUMAN,
        DWARF,
        GNOME,
        NIGHTELF
    }
    public enum UnitRace {
        GNOLL,
        SKELETON,
        MONGREL,
        OGRE
    }
    public enum UnitType {
        HUMANOID,
        BEAST,
        ELEMENTAL,
        UNDEAD,
        BUILDING,
        CRITTER,
        DEMON
    }
    public enum MOVEMENT {
        RUNNING,
        WALKING,
    }
    // struct HeroRace {
    //     string id;
    //     string race;
    // }
    // struct HeroRaces {
    //     HeroRace heroRace;
    // }

    public enum ACTION_STATE {
        IDLE,
        MOVING,
        ATTACKING,
        CASTING,
        DEAD,
        READY
    }

    public interface AttributeIF {
        void SetName(string name);
        string GetName();
        void SetHealth(int health);
        int GetHealth();
        void SetEnergy(float energy);
        float GetEnergy();
        void SetStrength(int strength);
        int GetStrength();
        void SetIntellect(int intellect);
        int GetIntellect();
        void SetAgility(int agility);
        int GetAgility();
        void SetStamina(int stamina);
        int GetStamina();
        void SetSpirit(int spirit);
        int GetSpirit();
    }
}
