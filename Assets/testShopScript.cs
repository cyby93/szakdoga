﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class testShopScript : MonoBehaviour {
    [SerializeField] private GameObject windowPrefab;

    private void OnMouseOver() {
        if (Input.GetMouseButtonDown(1)) {
            PanelManager.instance.OpenPanel(windowPrefab);
        }
    }
}