﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VendorSystem : MonoBehaviour
{
    private int availableGold = 0;
    [SerializeField] private Inventory playerInventory;
    [SerializeField] private IntEvent goldEvent;

    public void setAvailableGold(int gold)
    {
        availableGold += gold;
    }

    public void purchaseItem(ItemSlot itemSlot)
    {
        if (itemSlot.Item.SellPrice < availableGold || true)
        {
            playerInventory.ItemContainer.AddItem(new ItemSlot(itemSlot.Item, 1));
            goldEvent.Raise(0 - itemSlot.Item.SellPrice);
        }
        else
        {
            Debug.LogError("Got not enough gold.");
        }
    }
}