﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemEquipManager : Singleton<ItemEquipManager>
{
    protected ItemEquipManager()
    {
    }

    [SerializeField] private Inventory playerEquipment;
    [SerializeField] private Inventory playerInventory;

    public event Action<ArmorItem> OnItemEquip;
    public event Action<ArmorItem> OnItemUnequip;

    public void EquipItem(ArmorItem modifier)
    {
        OnItemEquip?.Invoke(modifier);
    }

    public virtual void UnEquipItem(ArmorItem modifier)
    {
        OnItemUnequip?.Invoke(modifier);
    }

    public void handleItemUseEvent(int index)
    {
        if (CanEquipArmor(playerInventory.GetItemAt(index) as ArmorItem))
        {
            playerEquipment.AddItemToInventory(playerInventory.GetItemAt(index));
            playerInventory.RemoveItemFromInventory(index);
        }
    }

    public void handleItemUnuseEvent(int index)
    {
        playerInventory.AddItemToInventory(playerEquipment.GetItemAt(index));
        playerEquipment.RemoveItemFromInventory(index);
    }

    private bool CanEquipArmor(ArmorItem item)
    {
        // if (item.Type == ItemType.Equipment && playerEquipment.GetItemAt((int) item.slotType) == null)
        if (item.Type == ItemType.Equipment)
        {
            return true;
        }

        Debug.LogError("Cant equip that item, since its not allowed");
        return false;
    }
}