﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class UIDataSyncer : MonoBehaviour {
    [SerializeField] private Text waveCounterText;
    public int waveCount;

    void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // called second
    void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        // Debug.Log("OnSceneLoaded: " + scene.name);
        if (scene.buildIndex == 2) {

            initStuff();
        }
    }

    private void Start() {


    }
    private void initStuff() {
        waveCounterText = GameObject.Find("Waves").GetComponent<Text>();

    }
    private void Update() {
        if(waveCounterText)
        waveCounterText.text = "asd: " + waveCount;
    }
    public void SetWaveCount(int number) {
        waveCount = number;
    }
}