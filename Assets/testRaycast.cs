﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.AI;

public class testRaycast : MonoBehaviour
{
    private Camera cam;
    void Start()
    {
        cam = Camera.main;

    }

    // Update is called once per frame
    void Update()
    {
        HandleRightClick();
        if (Input.GetKeyDown(KeyCode.O)) {
            DrawCorners();
        }
    }

    private void DrawCorners() {
        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        for (int i = 0; i < agent.path.corners.Length; i++) {
            Debug.DrawLine(agent.path.corners[i], agent.path.corners[i+1], Color.red, 5f);
        }
    }
    private void HandleRightClick() {
        if (Input.GetMouseButtonDown(1)) {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            int groundLayerMask = 1 << LayerMask.NameToLayer("Ground");
            int enemyLayerMask = 1 << LayerMask.NameToLayer("Enemy");
            int combinedLayerMask = groundLayerMask | enemyLayerMask;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, combinedLayerMask)) {
                GetComponent<NavMeshAgent>().SetDestination(hit.point);
            }
        }
    }

}
