﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class CharacterCanvas : MonoBehaviour
{
    private GameObject unitGameObject;
    private Unit unit;
    [SerializeField] private Image healthbar;
    [SerializeField] private Image energyBar;
    private int maxHealth, maxEnergy;


    private void Awake() {
        unitGameObject = gameObject.transform.parent.gameObject; 
        if (unitGameObject.GetComponent<PhotonView>().IsMine) {
            gameObject.SetActive(false);
        }
        unit = unitGameObject.GetComponent<Unit>();
    }

    void Update()
    {
        if(transform.rotation != Camera.main.transform.rotation) transform.rotation = Camera.main.transform.rotation;
        if(gameObject.transform.parent.GetComponent<Unit>().UnitStats.GetHealth() <= 0) {
            DestroyImmediate(gameObject);
        }

        UpdateHealth();
        UpdateEnergy();
    }

    private void UpdateHealth() {
        healthbar.fillAmount = (unit.UnitStats.GetHealth() / (float)unit.UnitStats.maxHealth);
    }
    private void UpdateEnergy() {
        energyBar.fillAmount = (unit.UnitStats.GetEnergy() / (float)unit.UnitStats.maxEnergy);
    }
}