﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownCenterFlameActivator : MonoBehaviour {
    
    [SerializeField] private List<GameObject> flames;
    private UnitStats stats;
    
    void Start() {
        stats = GetComponent<UnitStats>();
        InvokeRepeating("SetFlameActive", 0, 1f);
    }
    
    private void SetFlameActive() {
        float missingHealthAmount = (1f - (stats.GetHealth() / (float) stats.maxHealth)) * 100;
        float onePiece = 100 / flames.Count + 1;
        float activeFlamesNumber = missingHealthAmount / onePiece;
        for (var i = 0; i <= activeFlamesNumber; i++) {
            flames[i].SetActive(true);
        }
    }
}
