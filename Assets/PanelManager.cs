﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelManager : MonoBehaviour {
    public static PanelManager instance;
    [SerializeField] private Transform openedPanelParent;
    [SerializeField] private List<PanelObject> panels;
    private GameObject currentlyOpenedPanel;
    private List<KeyCode> alreadySetupKeys = new List<KeyCode>();
    private void Awake() {
        instance = this;
    }
    void Start()
    {
        InitKeyCodes();
        closeAllPanel();
    }

    void Update() {
        if (currentlyOpenedPanel && Input.GetKeyDown(KeyCode.Escape)) {
            Destroy(currentlyOpenedPanel);
        }

        onHotkeyPressed();
        
    }

    public void OpenPanel(GameObject panelPrefab) {
        currentlyOpenedPanel = Instantiate(panelPrefab, openedPanelParent.transform);
    }

    private void onHotkeyPressed()
    {
        foreach (var panel in panels)
        {
            if (Input.GetKeyDown(panel.key))
            {
                panel.togglePanel();
            }
        }
    }
    
    private void InitKeyCodes()
    {
        foreach (PanelObject panelObject in panels)
        {
            alreadySetupKeys.Add(panelObject.key);
        }
    }
    
    private void closeAllPanel()
    {
        foreach (PanelObject panelObject in panels)
        {
            panelObject.panel.gameObject.SetActive(false);
        }
    }
}
