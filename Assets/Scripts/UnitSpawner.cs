﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class UnitSpawner : NetworkBehaviour {
    [SerializeField]
    private GameObject enemyPrefab;

    private void Start() {
        StartCoroutine(spawnUnit(3f));

    }
    void Update() {
        if (Input.GetKeyDown(KeyCode.H)) {
            CmdSpawnEnemy();
        }

    }

    [Command]
    void CmdSpawnEnemy() {
        if (enemyPrefab == null) return;
        GameObject go = Instantiate(enemyPrefab);
        Debug.Log("Spawning enemy: " + enemyPrefab);
        NetworkServer.Spawn(go);
    }

    private IEnumerator spawnUnit(float seconds) {
        while (true) {
            yield return new WaitForSeconds(seconds);
            CmdSpawnEnemy();
        }
    }
}