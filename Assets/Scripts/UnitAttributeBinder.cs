﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UnitAttributeBinder : MonoBehaviour {
    [SerializeField] private GameObject Attributes;
    [SerializeField] private GameObject Healthbar, EnergyBar, Name, ExperienceBar;
    [SerializeField] private Text CurrentLevelText;
    private PhotonNetworkPlayer _photonNetworkPlayer;
    private Unit localPlayerUnit;

    void Update() {
        SetOrUnsetLocalUnit();
        if (hasLocalPlayerUnitData()) {
            UpdateUnitInterface();
        }
    }

    private void SetOrUnsetLocalUnit() {
        if (_photonNetworkPlayer && isPlayerUnitExists() && !localPlayerUnit) {
            localPlayerUnit = _photonNetworkPlayer.GetMyPlayerUnit().GetComponent<Unit>();
        } else {
            localPlayerUnit = null;
            if (isLocalPlayerObjectExists()) _photonNetworkPlayer = GameObject.Find("LocalPlayerObject").GetComponent<PhotonNetworkPlayer>();
        }
    }

    private void UpdateUnitInterface() {
        UpdateHealth();
        UpdateEnergy();
        UpdateExperienceAndLevel();
    }
    private void UpdateHealth() {
        Healthbar.GetComponent<Image>().fillAmount = (float) localPlayerUnit.UnitStats.GetHealth() / (float) localPlayerUnit.UnitStats.maxHealth;
    }
    private void UpdateEnergy() {
        EnergyBar.GetComponent<Image>().fillAmount = (float) localPlayerUnit.UnitStats.GetEnergy() / (float) localPlayerUnit.UnitStats.maxEnergy;
    }
    private void UpdateExperienceAndLevel() {
        ExperienceBar.GetComponent<Image>().fillAmount = currentExperience() / requiredXpForNextLevelUp();
        CurrentLevelText.text = "Level " + localPlayerUnit.UnitStats.level;
    }

    private bool hasLocalPlayerUnitData() {
        return localPlayerUnit != null;
    }
    private bool isLocalPlayerObjectExists() {
        return GameObject.Find("LocalPlayerObject");
    }
    private bool isPlayerUnitExists() {
        return _photonNetworkPlayer.GetMyPlayerUnit();
    }

    private float experienceForNextLevel() {
        return (float)Constants.ExperienceLevels[localPlayerUnit.UnitStats.level];
    }
    private float experienceForLastLevel() {
        return (float)Constants.ExperienceLevels[localPlayerUnit.UnitStats.level - 1];
    }
    private float requiredXpForNextLevelUp() {
        return experienceForNextLevel() - experienceForLastLevel();
    }
    private float currentExperience() {
        return localPlayerUnit.UnitStats.experience - experienceForLastLevel();
    }
}