﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class VendorSlot : ItemSlotUI, IPointerClickHandler
{
    [SerializeField] private VendorInventory vendorItems = null;
    [SerializeField] private VendorSystem vendorSystem;

    public override ItemObject SlotItem {
        get { return ItemSlot.Item; }
        set { }
    }
    public ItemSlot ItemSlot => vendorItems.ItemContainer.GetSlotByIndex(SlotIndex);
    
    public override void OnDrop(PointerEventData eventData) {
        throw new System.NotImplementedException();
    }

    public override void UpdateSlotUI() {
        if (ItemSlot.Item == null) {
            EnableSlotUI(false);
            return;
        }

        EnableSlotUI(true);
        itemIconImage.sprite = ItemSlot.Item.Icon;

    }
    
    public void OnPointerClick(PointerEventData eventData) {
        if (eventData.button == PointerEventData.InputButton.Right && ItemSlot.Item) {
            vendorSystem.purchaseItem(ItemSlot);
        }
    }
}
