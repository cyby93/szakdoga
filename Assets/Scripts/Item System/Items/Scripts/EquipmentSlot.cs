﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class EquipmentSlot : ItemSlotUI {
    [SerializeField] private Inventory equipment = null;
    [SerializeField] private EquipmentSlotType slotType;
    public override ItemObject SlotItem {
        get { return ItemSlot.Item; }
        set { }
    }

    public ItemSlot ItemSlot => equipment.ItemContainer.GetSlotByIndex(SlotIndex);

    protected override void Start()
    {
        base.Start();
        slotType = (EquipmentSlotType) SlotIndex;
    }
    public override void OnDrop(PointerEventData eventData) {
        ItemDragHandler itemDragHandler = eventData.pointerDrag.GetComponent<ItemDragHandler>();

        // if (itemDragHandler == null) return;
        // if ((itemDragHandler.ItemSlotUI as InventorySlot) != null) {
        //     equipment.ItemContainer.Swap(itemDragHandler.ItemSlotUI.SlotIndex, SlotIndex);
        // }
        //removed the functionality
    }

    public override void UpdateSlotUI()
    {
        if (ItemSlot.Item == null) {
            EnableSlotUI(false);
            return;
        }

        EnableSlotUI(true);
        itemIconImage.sprite = ItemSlot.Item.Icon;
    }
}