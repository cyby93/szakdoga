﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemDragHandler : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler {
    [SerializeField] protected ItemSlotUI itemSlotUI = null;
    [SerializeField] protected ItemObjectEvent onMouseHoverStart = null;
    [SerializeField] protected VoidEvent onMouseHoverEnd = null;
    private CanvasGroup canvasGroup = null;
    private Transform originalParent = null;
    private bool isHovering;

    public ItemSlotUI ItemSlotUI => itemSlotUI;

    private void Start() => canvasGroup = GetComponent<CanvasGroup>();

    private void OnDisable() {
        if (isHovering) {
            //raise event
            isHovering = false;
        }
    }

    public virtual void OnPointerDown(PointerEventData eventData) {
        if (IsLeftButton(eventData)) {
            onMouseHoverEnd.Raise();
            originalParent = transform.parent;
            transform.SetParent(transform.parent.parent);
            canvasGroup.blocksRaycasts = false;
        }
    }

    public virtual void OnDrag(PointerEventData eventData) {
        if (IsLeftButton(eventData)) {
            transform.position = Input.mousePosition;
        }
    }

    public virtual void OnPointerUp(PointerEventData eventData) {
        if (IsLeftButton(eventData)) {
            transform.SetParent(originalParent);
            transform.localPosition = Vector3.zero;
            canvasGroup.blocksRaycasts = true;
        }
    }

    public virtual void OnPointerEnter(PointerEventData eventData) {
        onMouseHoverStart.Raise(ItemSlotUI.SlotItem);
        isHovering = true;
    }

    public virtual void OnPointerExit(PointerEventData eventData) {
        onMouseHoverEnd.Raise();
        isHovering = false;
    }

    private bool IsLeftButton(PointerEventData eventData) {
        return eventData.button == PointerEventData.InputButton.Left;
    }
}
