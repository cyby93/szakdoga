﻿using System;

[Serializable]
public struct ItemSlot {
   public InventoryItem Item;
   public int Quantity;

   public ItemSlot(InventoryItem item, int quantity) {
      Item = item;
      Quantity = quantity;
   }

   public static bool operator ==(ItemSlot a, ItemSlot b) {
      return a.Equals(b);
   }
   
   public static bool operator !=(ItemSlot a, ItemSlot b) {
      return !a.Equals(b);
   }
}
