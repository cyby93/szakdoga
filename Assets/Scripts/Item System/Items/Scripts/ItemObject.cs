﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UIElements;

public abstract class ItemObject : ScriptableObject {
    [Header("Basic Info")] 
    [SerializeField] private new string name = "New Default Item";
    [SerializeField] private Sprite icon;
    [SerializeField] private ItemType type;

    public string Name => name;
    public Sprite Icon => icon;
    public ItemType Type => type;

    public abstract string GetInfoDisplayText();
}

public enum ItemType {
    Consumable,
    Equipment,
    Default
}