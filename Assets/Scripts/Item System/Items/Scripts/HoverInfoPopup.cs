﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HoverInfoPopup : MonoBehaviour {
    [SerializeField] private GameObject popupCanvasObject = null;

    private TextMeshProUGUI itemName;
    private TextMeshProUGUI itemDescription;
    private TextMeshProUGUI itemPrice;

    private void Start()
    {
        itemDescription = popupCanvasObject.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
    }
    public void ShowTooltip(ItemObject itemObject)
    {
        // itemName.text = itemObject.Name;
        itemDescription.text = itemObject.GetInfoDisplayText();
        // itemPrice.text = "Sell price: " + itemObject.SellPrice;
        popupCanvasObject.SetActive(true);
    }

    public void HideTooltip() => popupCanvasObject.SetActive(false);
    
}
