﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class InventorySlot : ItemSlotUI {
    [SerializeField] private Inventory inventory = null;
    [SerializeField] private TextMeshProUGUI itemQuantityText = null;

    public override ItemObject SlotItem {
        get { return ItemSlot.Item; }
        set { }
    }

    public ItemSlot ItemSlot => inventory.ItemContainer.GetSlotByIndex(SlotIndex);

    public override void OnDrop(PointerEventData eventData) {
        ItemDragHandler itemDragHandler = eventData.pointerDrag.GetComponent<ItemDragHandler>();

        if (itemDragHandler == null) return;
        if ((itemDragHandler.ItemSlotUI as InventorySlot) != null) {
            inventory.ItemContainer.Swap(itemDragHandler.ItemSlotUI.SlotIndex, SlotIndex);
        }
    }

    public override void UpdateSlotUI() {
        if (ItemSlot.Item == null) {
            EnableSlotUI(false);
            return;
        }

        EnableSlotUI(true);
        itemIconImage.sprite = ItemSlot.Item.Icon;
        itemQuantityText.text = ItemSlot.Quantity > 1 ? ItemSlot.Quantity.ToString() : "";
    }
    
    protected override void EnableSlotUI(bool enable) {
        base.EnableSlotUI(enable);
        itemQuantityText.enabled = enable;
    }
}