﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "New Armor Item", menuName = "Items/Armor")]

public class ArmorItem : InventoryItem
{
    [Header("Armor Data")] 
    [SerializeField] private string useText = "Shiny armor, i guess...";

    [SerializeField] public EquipmentSlotType slotType;
    
    [Header("Given stats")] 
    [SerializeField] private ModificationDataBlock modifiers;
    public override string GetInfoDisplayText() {
        StringBuilder builder = new StringBuilder();
        builder.Append(Name).AppendLine();
        builder.Append("<color=yellow>Equip: ").Append(useText).Append("</color>").AppendLine();
        builder.Append("Sell Price: ").Append(SellPrice).Append(" Gold");

        return builder.ToString();
    }

    public ModificationDataBlock Modifiers => modifiers;
}

[Serializable]
public struct ModificationDataBlock {
     public StatModifier stamina;
    [SerializeField] public StatModifier intellect;
    [SerializeField] public StatModifier agility;
}

public enum EquipmentSlotType
{
    HEAD, BODY, FEET, WEAPON, TRINKET
}