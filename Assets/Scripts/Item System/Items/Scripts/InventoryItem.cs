﻿using UnityEngine;

public abstract class InventoryItem : ItemObject {
    [Header("Item Data")] 
    [SerializeField] [Min(0)] private int sellPrice = 1;
    [SerializeField] [Min(1)] private int maxStack = 1;

    public int SellPrice => sellPrice;
    public int MaxStack => maxStack;

}
