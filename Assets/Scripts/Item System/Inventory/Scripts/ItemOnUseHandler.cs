﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemOnUseHandler : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private IntEvent onInventoryItemRightClicked;
    [SerializeField] private ItemSlotUI inventorySlot;

    public void OnPointerClick(PointerEventData eventData) {
        if (eventData.button == PointerEventData.InputButton.Right && inventorySlot)
        {
            onRightClick();
        }
    }

    public void onRightClick()
    {
        onInventoryItemRightClicked.Raise(inventorySlot.SlotIndex);
    }

}
