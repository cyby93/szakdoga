﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Inventory", menuName = "Items/Inventory")]
public class Inventory : ScriptableObject {
    [SerializeField] private VoidEvent onInventoryItemsUpdated = null;
    [SerializeField] private ItemSlot testItemSlot = new ItemSlot();
    [SerializeField] private int size;
    public ItemContainer ItemContainer { get; set; }

    public void OnEnable()
    {
        ItemContainer = new ItemContainer(size);
        ItemContainer.OnItemsUpdated += onInventoryItemsUpdated.Raise;
    }

    private void OnDisable() => ItemContainer.OnItemsUpdated -= onInventoryItemsUpdated.Raise;

    [ContextMenu("Test Add")]
    public void TestAdd() {
        ItemContainer.AddItem(testItemSlot);
    }

    public void AddItemToInventory(InventoryItem item) {
        ItemContainer.AddItem(new ItemSlot(item, 1));
    }

    public void RemoveItemFromInventory(int item)
    {
        ItemContainer.RemoveAt(item);
    }

    public InventoryItem GetItemAt(int index)
    {
        return ItemContainer.GetSlotByIndex(index).Item;
    }
}
