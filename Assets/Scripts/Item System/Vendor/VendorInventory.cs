﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Vendor Inventory", menuName = "Items/Vendor Inventory")]
public class VendorInventory : ScriptableObject {
    [SerializeField] private VoidEvent onVendorItemsUpdated = null;
    public ItemContainer ItemContainer { get; } = new ItemContainer(25);

    [Header("Purchasable Items")] [SerializeField]
    public List<ItemSlot> items = new List<ItemSlot>();

    private void OnEnable() {
        items.ForEach(item => {
            if (item.Item)
                ItemContainer.AddItem(item);
        });
        onVendorItemsUpdated.Raise();
    }
    }