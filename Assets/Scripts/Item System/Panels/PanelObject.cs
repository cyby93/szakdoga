﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PanelObject 
{
    [SerializeField] private GameObject panelGameObject;
    [SerializeField] private KeyCode hotkey;
    
    public GameObject panel {
        get { return panelGameObject; }
    }

    public KeyCode key => hotkey;

    public void togglePanel()
    {
        panelGameObject.SetActive(!panelGameObject.activeSelf);
    }
}
