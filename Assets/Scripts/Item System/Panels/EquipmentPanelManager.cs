﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentPanelManager : MonoBehaviour
{
    // Start is called before the first frame update
    
    private void OnEnable() {
        ItemEquipManager.Instance.OnItemEquip += addItemToEquipment;
        ItemEquipManager.Instance.OnItemUnequip += removeItemFromEquipment;
    }

    private void OnDisable() {
        ItemEquipManager.Instance.OnItemEquip -= addItemToEquipment;
        ItemEquipManager.Instance.OnItemUnequip -= removeItemFromEquipment;
    }

    private void addItemToEquipment(ItemObject item)
    {
        Debug.Log("item equipped: " + item);
    }
    private void removeItemFromEquipment(ItemObject item)
    {
        Debug.Log("item removed: " + item);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
