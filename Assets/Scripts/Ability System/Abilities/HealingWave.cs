﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Photon.Pun;
using UnityEngine;

[CreateAssetMenu(fileName = "New HealingWave", menuName = "Abilities/HealingWave", order = 1)]
public class HealingWave : Ability {
    private GameObject spellObject;

    public override void TriggerAbility(GameObject caster, Vector3 targetPosition) {
        Initialize(caster, targetPosition);
    }

    public override void Initialize(GameObject caster, Vector3 targetPosition) {
        if ( caster.GetComponent<PhotonView>().IsMine) {
            spellObject = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "Abilities", abilityPrefab.name),
                                                    targetPosition, caster.transform.rotation);
            spellObject.GetComponent<HealingWaveBehavior>().InitAbilityObject(data);
        }
        
        
    }

}