﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingWaveBehavior : MonoBehaviour
{
    
    public void InitAbilityObject(AbilityData data) {
        StartCoroutine(HealAlliesInArea((int)data.heal));
    }

    private IEnumerator HealAlliesInArea(int amount) {
        yield return new WaitForSeconds(0.5f);
        Heal(amount);
        yield return new WaitForSeconds(1f);
        DestroyImmediate(gameObject);
    }
    
    private void Heal(int amount) {
        foreach (var ally in GetAllAllyNearby()) {
            ally.HealDamage(amount);
        }
    }

    private List<Unit> GetAllAllyNearby() {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 3f);
        List<Unit> allies = new List<Unit>();
        foreach (var collider in hitColliders) {
            if (collider.gameObject.CompareTag("Player")) allies.Add(collider.gameObject.GetComponent<Unit>());
        }
        return allies;
    }

    
}
