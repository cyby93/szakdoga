﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Photon.Pun;
using UnityEngine;

[CreateAssetMenu(fileName = "New Fireball", menuName = "Abilities/Fireball", order = 1)]
public class Fireball : Ability {
    private GameObject spellObject;

    public override void TriggerAbility(GameObject caster, Vector3 targetPosition) {
        Initialize(caster, targetPosition);
    }

    public override void Initialize(GameObject caster, Vector3 targetPosition) {
        Vector3 spawnPoint = caster.transform.position + new Vector3(0, 1, 0);
        if ( caster.GetComponent<PhotonView>().IsMine) {
            spellObject = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "Abilities", abilityPrefab.name),
                                                    spawnPoint, caster.transform.rotation);
            spellObject.GetComponent<FireballBehaviour>().InitAbilityObject(data);
        }
        
        
    }

}