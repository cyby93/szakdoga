﻿using UnityEngine;
using System.Collections;


public class AbilityBehaviour
{
    protected BehaviourStartTime startTime;
    protected GameObject abilityPrefab;
    protected IEnumerator behaviourToPerform;

    public virtual void PerformBehaviour(float _speed)
    {
        Debug.LogError("NEED TO OVERWRITE!");
    }
    public virtual IEnumerator testEnumeratorWrapper() {
        return behaviourToPerform;
    }

    public enum BehaviourStartTime {
        START,
        MIDDLE,
        END
    }
}
