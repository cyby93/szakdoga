﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="Accesories/Spellbook")]
public class SpellBookScriptable : ScriptableObject
{
    [SerializeField] public List<Ability> abilities;

    // public List<KeyValuePair<SlotKey, Ability>> abilityKeys = new List<KeyValuePair<SlotKey, Ability>>();
    
    public void InitializeSpellbook() {
        // abilityKeys.Add(new KeyValuePair<SlotKey, Ability>(SlotKey.E, abilities[0]));
        // MUCH WORK TO DO... fill somehow the key-value pairs
    }
}
