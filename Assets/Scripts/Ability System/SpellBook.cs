﻿using System.Security.Cryptography.X509Certificates;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellBook : MonoBehaviour
{
    [SerializeField] public List<AbilitySlot> abilities;

    public SpellBook()
    {
        abilities = new List<AbilitySlot>();
    }

    public Ability getAbility(SlotKey _key)
    {
        return this.findAbility(_key).ability;
    }
    public void addAbility(SlotKey _key, Ability _ability)
    {
        if (!isSlotTaken(_key))
        {
            abilities.Add(new AbilitySlot(_key, _ability));
        }
    }

    private bool isSlotTaken(SlotKey _key)
    {
        foreach (var ability in abilities)
        {
            if (ability.key == _key) return true;
        }
        return false;
    }
    private AbilitySlot findAbility(SlotKey _key)
    {
        foreach (var ability in abilities)
        {
            
            if (ability.key == _key) return ability;
        }
        return null;
    }
    private AbilitySlot findAbility(Ability _ability)
    {
        foreach (var ability in abilities)
        {
            
            if (ability.ability == _ability) return ability;
        }
        return null;
    }

}
