﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class Ability : ScriptableObject {
    public string abilityName = "Default Name";
    public Sprite icon;
    public AbilityData data;
    public SlotKey hotkey;
    public GameObject abilityPrefab;
    protected List<AbilityBehaviour> abilityBehaviours = new List<AbilityBehaviour>();

    public abstract void Initialize(GameObject caster, Vector3 targetPosition);
    public abstract void TriggerAbility(GameObject caster, Vector3 targetPosition);
}

[Serializable]
public struct AbilityData {
    public float cost;
    public float cooldown;
    public float speed;
    public float castTime;
    public float damage;
    public float heal;
}