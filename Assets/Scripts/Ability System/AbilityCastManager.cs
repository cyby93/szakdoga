﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Networking;

public class AbilityCastManager : MonoBehaviour {
    private Unit unit;
    private Unit targetUnit;
    private Ability selectedAbility;
    private PhotonView photonView;

    private delegate void AbilityCastDelegate(GameObject casterGameObject, Vector3 targetPosition);

    private AbilityCastDelegate delegatedAbilityToCast;
    private IEnumerator casting;
    private float remainingCastTime;

    private void Start() {
        unit = gameObject.GetComponent<Unit>();
        photonView = GetComponent<PhotonView>();
    }

    private void Update() {
        if (!photonView.IsMine) return;
        if (AbilityCastedByLeftClick()) {
            gameObject.GetComponent<UnitActionExecuter>().StartCastingAbility(GetClickPosition());
        }
    }

    public void SelectAbility(SlotKey key) {
        Ability clickedAbility = SpellBookUtility.GetAbility(unit.spellBook, key);
        if (delegatedAbilityToCast == null && HasEnoughEnergyForAbility(clickedAbility)) {
            selectedAbility = clickedAbility;
            photonView.RPC("RPC_SelectAbility", RpcTarget.All, key);
            CursorController.Instance.SetActiveCursor();
        }
    }

    [PunRPC]
    private void RPC_SelectAbility(SlotKey key) {
        selectedAbility = SpellBookUtility.GetAbility(unit.spellBook, key);
    }

    public void CancelCasting() {
        selectedAbility = null;
        delegatedAbilityToCast = null;
        if (casting != null) {
            StopCoroutine(casting);
        }

        CursorController.Instance.SetNormalCursor();
    }

    public void StartCastingAbility(Vector3 targetPosition) {
        if (selectedAbility != null) {
            CursorController.Instance.SetNormalCursor();
            TurnCharacterToCastDirection(targetPosition);
            delegatedAbilityToCast = selectedAbility.TriggerAbility;
            casting = CastAbility(selectedAbility, targetPosition);
            selectedAbility = null;
            StartCoroutine(casting);
        }
    }

    IEnumerator CastAbility(Ability ability, Vector3 targetPosition) {
        Ability tmpAbility = ability;
        for (remainingCastTime = tmpAbility.data.castTime; remainingCastTime > 0; remainingCastTime -= Time.deltaTime)
            yield return null;
        SubstractEnergyCostFromUnit(tmpAbility);
        delegatedAbilityToCast(gameObject, targetPosition);
        delegatedAbilityToCast = null;
    }

    private void TurnCharacterToCastDirection(Vector3 targetPosition) {
        Vector3 relativePos = targetPosition - unit.transform.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        unit.transform.rotation = rotation;
    }
    
    private bool HasEnoughEnergyForAbility(Ability ability) {
        return unit.UnitStats.GetEnergy() >= ability.data.cost;
    }

    public bool IsCasting() {
        return delegatedAbilityToCast != null;
    }

    public bool HasSelectedAbility() {
        return selectedAbility != null;
    }

    public bool IsAboutToFinishCasting() {
        return remainingCastTime <= 0.3f;
    }

    private bool AbilityCastedByLeftClick() {
        return selectedAbility != null && Input.GetMouseButtonDown(0);
    }

    private Vector3 GetClickPosition() {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit)) {
            return hit.point;
        }

        return gameObject.transform.forward;
    }

    private void SubstractEnergyCostFromUnit(Ability ability) {
        unit.UnitStats.SetEnergy(unit.UnitStats.GetEnergy() - (int) ability.data.cost);
    }
}