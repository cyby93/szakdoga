﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]
public class Projectile : AbilityBehaviour
{
    private Rigidbody rBody;
    private SphereCollider sphereCollider;

    public Projectile(GameObject _abilityPrefab, float _speed) {
        this.abilityPrefab = _abilityPrefab;
        abilityPrefab.GetComponent<Rigidbody>().useGravity = false;
        abilityPrefab.GetComponent<SphereCollider>().isTrigger = true;
        behaviourToPerform = moveProjectile(_speed);
    }
    // private void Awake()
    // {
    //     rBody = GetComponent<Rigidbody>();
    //     sphereCollider = GetComponent<SphereCollider>();
    // }
    // private void Start()
    // {
    //     sphereCollider.isTrigger = true;
    //     rBody.useGravity = false;
    // }
    public override void PerformBehaviour(float _speed)
    {
        // StartCoroutine(moveProjectile(_speed));
        // abilityPrefab.StartCoroutine(moveProjectile(_speed));
        // Debug.Log("PerformedBehaviour of Projectile");
    }

    private IEnumerator moveProjectile(float _speed)
    {
        while (true)
        {
            abilityPrefab.GetComponent<Rigidbody>().MovePosition((Vector3)abilityPrefab.transform.position + (abilityPrefab.transform.forward * Time.deltaTime * _speed));
            Collider[] hitColliders = Physics.OverlapSphere(abilityPrefab.transform.position, 0.5f);
            if(isEnemyNearby(hitColliders)) GameObject.Destroy(abilityPrefab);
            yield return null;
        }
    }

    // private void OnTriggerEnter(Collider other)
    // {
    //     if(other.gameObject.tag == "Building") {
    //         Destroy(gameObject);
    //     }
    //     if(other.gameObject.tag == "Enemy") {
    //         dealDamage(other.gameObject);
    //         Destroy(gameObject);
    //     }
    // }
    private bool isEnemyNearby(Collider[] _colliders) {
        return _colliders.Length > 0 && hasEnemyCollider(_colliders);
    }
    private bool hasEnemyCollider(Collider[] _colliders){
        foreach (var collider in _colliders)
        {
            if(collider.gameObject.tag == "Enemy") return true;
        }
        return false;
    }
    private void dealDamage(GameObject _enemy) {
        _enemy.GetComponent<Unit>().TakeDamage(null, 25);
    }

}
