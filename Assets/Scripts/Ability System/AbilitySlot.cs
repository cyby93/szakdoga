﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AbilitySlot 
{
    [SerializeField] public SlotKey? key = SlotKey.Q;
    [SerializeField] public Ability ability { set; get; }

    public AbilitySlot(SlotKey _key, Ability _ability) {
        key = _key;
        ability = _ability;
    }
}

[Serializable]
public enum SlotKey {
    Q, W, E, R, A, S, D, F
}