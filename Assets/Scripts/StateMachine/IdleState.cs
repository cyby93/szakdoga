﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : BaseState
{
    private Unit unit;
    
    public IdleState(Unit _unit) : base(_unit.gameObject)
    {
        unit = _unit;
    }

    public override Type Tick() {
        unit.AnimController.SetBool("IdleState", false);
        unit.AnimController.SetBool("CastAbility", false);

        if (unit.UnitStats.GetHealth() <= 0)
        { return typeof(DeadState); }
        
        if(unit.Destination != null)
        { return typeof(MoveState); }
        
        if (unit.IsUnitCasting())
        { return typeof(ReadyCastState);  }
        
        if(unit.Target != null)
        { return typeof(AttackState); }
        
        unit.SetNavMeshAgentEnabled(false);
        unit.AnimController.SetBool("IdleState", true);

        return null;
    }
}
