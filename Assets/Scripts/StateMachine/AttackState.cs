﻿using System;
using System.Collections;
using UnityEngine;

public class AttackState : BaseState {
    private Unit unit;

    public AttackState(Unit _unit) : base(_unit.gameObject) {
        unit = _unit;
    }

    public override Type Tick() {
        unit.AnimController.SetBool("AttackState", false);
        if (unit.UnitStats.GetHealth() <= 0) {
            return typeof(DeadState);
        }

        if (unit.Target == null) {
            return typeof(IdleState);
        }
        if(unit.IsUnitCasting()){
            return typeof(ReadyCastState);
        }
        if (!unit.IsTargetInRange()) {
            return typeof(MoveState);
        }
        
        unit.SetNavMeshAgentEnabled(false);
        if (unit.GetNavMeshAgent().hasPath) unit.GetNavMeshAgent().ResetPath();
        
        Vector3 relativePos = unit.Target.transform.position - unit.transform.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        unit.transform.rotation = rotation;
        unit.onAttack.Invoke();
        
        unit.AnimController.SetBool("AttackState", true);
        return null;
    }

}