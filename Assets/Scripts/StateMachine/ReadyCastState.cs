﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ReadyCastState : BaseState {
    private Unit unit;

    public ReadyCastState(Unit _unit) : base(_unit.gameObject) {
        unit = _unit;
    }

    public override Type Tick() {
        unit.AnimController.SetBool("CastAbility", false);
        unit.AnimController.SetBool("FinishCasting", false);

        resetOtherStates();

        if (!unit.IsUnitCasting()) {
            return typeof(IdleState);
        }

        if (unit.gameObject.GetComponent<AbilityCastManager>().IsAboutToFinishCasting()) {
            unit.AnimController.SetBool("FinishCasting", true);
        }

        if (unit.UnitStats.GetHealth() <= 0) {
            return typeof(DeadState);
        }
        if (unit.Destination != null) {
            return typeof(MoveState);
        }
        
        unit.SetNavMeshAgentEnabled(false);
        unit.AnimController.SetBool("CastAbility", true);

        return null;
    }

    private void resetOtherStates() {
        if (unit.GetNavMeshAgent().hasPath) unit.GetNavMeshAgent().ResetPath();
    }
}