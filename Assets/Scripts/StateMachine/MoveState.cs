﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MoveState : BaseState {
    private Unit unit;

    public MoveState(Unit _unit) : base(_unit.gameObject) {
        unit = _unit;
    }

    public override Type Tick() {
        unit.AnimController.SetBool("MoveState", false);

        if (unit.UnitStats.GetHealth() <= 0 || unit.IsDead) {
            return typeof(DeadState);
        }

        if (unit.IsUnitCasting()) {
            return typeof(ReadyCastState);
        }

        if (unit.Target) {
            unit.Destination = unit.Target.transform.position;
        }

        if(unit.Target && unit.IsTargetInRange()) {
            unit.Destination = null;
            return typeof(IdleState);
        }
        
        unit.SetNavMeshAgentEnabled(true);
        
        
        if (!unit.GetNavMeshAgent().hasPath && unit.Destination == null) {
            return typeof(IdleState);
        }

        if (unit.Destination != null) {
            unit.GetNavMeshAgent().SetDestination(unit.Destination.Value);
            unit.Destination = null;
        }

        unit.AnimController.SetBool("MoveState", true);

        return null;
    }
}