using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ChaseState : BaseState {
    private Unit unit;

    public ChaseState(Unit _unit) : base(_unit.gameObject) {
        this.unit = _unit;
    }

    public override Type Tick() {
        unit.AnimController.SetBool("MoveState", false);
        // if Unit is dead
        if (unit.UnitStats.GetHealth() <= 0) {
            return typeof(DeadState);
        }
        if (unit.IsUnitCasting()) {
            return typeof(ReadyCastState);
        }
        if (!unit.Target) {
            return typeof(IdleState);
        }
        if (unit.Target && !unit.IsTargetInRange()) {
            unit.GetNavMeshAgent().SetDestination(unit.Target.transform.position);
            if (unit.IsTargetInRange()) {
                return typeof(AttackState);
            } 
        }
        unit.AnimController.SetBool("MoveState", true);
        return null;
    }

    private bool targetIsInRange() {
        return unit.GetNavMeshAgent().remainingDistance <= unit.GetNavMeshAgent().radius * 2;
    }
}