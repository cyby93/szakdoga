﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DeadState : BaseState
{
    private readonly Unit unit;

    public DeadState(Unit unit) : base(unit.gameObject)
    {
        this.unit = unit;
    }

    public override Type Tick() {
        unit.AnimController.SetBool("DeadState", false);

        if(unit.UnitStats.GetHealth() > 0){
            return typeof(IdleState);
        }

        // if(!_unit.isDead){
        //     _unit.Die();
        // }
        unit.AnimController.SetBool("DeadState", true);
        return null;
    }
}
