﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Managers {
    public class PhotonLobby : MonoBehaviourPunCallbacks {
        public static PhotonLobby lobby;

        public string roomName;
        public int roomSize = 12;
        public string playerName;
        public GameObject roomListingPrefab;
        public Transform roomsPanel;
        public GameObject loadingLayer;
        public GameObject menuLayer;
        public List<RoomInfo> roomListings;
        private Coroutine reconnectingAttempts;

        private void Awake() {
            lobby = this;
        }

        void Start() {
            PhotonNetwork.ConnectUsingSettings();
            roomListings = new List<RoomInfo>();
            if (reconnectingAttempts != null) StopCoroutine(reconnectingAttempts);
        }

        public override void OnConnectedToMaster() {
            if (reconnectingAttempts != null) {
                loadingLayer.transform.GetChild(1).GetComponent<Text>().text = "";
                StopCoroutine(reconnectingAttempts);
            }
            PhotonNetwork.NickName = RandomPlayerName();
            PhotonNetwork.AutomaticallySyncScene = false;
            StartCoroutine(DisableConnectingLayer());
            PhotonNetwork.JoinLobby();
        }

        public override void OnDisconnected(DisconnectCause cause) {
            base.OnDisconnected(cause);
            ActivateLoadingLayer("Reconnecting...", 
                                 "Lost connection, but trying to reconnect every 3 sec...");
            reconnectingAttempts = StartCoroutine(Reconnecting());
        }

        private IEnumerator Reconnecting() {
            yield return new WaitForSeconds(3f);
            PhotonNetwork.ConnectUsingSettings();
        }

        public override void OnRoomListUpdate(List<RoomInfo> roomList) {
            base.OnRoomListUpdate(roomList);
            int tempIndex;
            foreach (RoomInfo room in roomList) {
                if (roomListings != null) {
                    tempIndex = roomListings.FindIndex(ByName(room.Name));
                }
                else {
                    tempIndex = -1;
                }

                if (tempIndex != -1) {
                    roomListings.RemoveAt(tempIndex);
                    Destroy(roomsPanel.GetChild(tempIndex).gameObject);
                }
                else {
                    roomListings.Add(room);
                    ListRoom(room);
                }
            }
        }

        static System.Predicate<RoomInfo> ByName(string name) {
            return delegate(RoomInfo info) { return info.Name == name; };
        }

        private void ListRoom(RoomInfo room) {
            if (room.IsOpen && room.IsVisible) {
                GameObject tempListing = Instantiate(roomListingPrefab, roomsPanel);
                RoomListItem tempButton = tempListing.GetComponent<RoomListItem>();
                tempButton.roomName = room.Name;
                tempButton.roomSize = room.MaxPlayers;
                tempButton.SetRoom();
            }
        }

        public void CreateRoom() {
            RoomOptions roomOptions = new RoomOptions() {
                IsVisible = true, IsOpen = true, MaxPlayers = (byte) roomSize
            };
            if (roomName == null || roomName == "") roomName = RandomRoomName();
            ActivateLoadingLayer("Creating room...", "Please wait");
            StartCoroutine(CreateRoomWithDelay(roomOptions));
        }

        private IEnumerator CreateRoomWithDelay(RoomOptions roomOptions) {
            yield return  new WaitForSeconds(0.2f);
            PhotonNetwork.CreateRoom(roomName, roomOptions);
            PhotonNetwork.LoadLevel(1);
        }

        public override void OnCreateRoomFailed(short returnCode, string message) {
            Debug.Log("Trued to create a new room but failed, there must be a room with the same name!");
        }

        public void OnRoomNameChanged(string nameIn) {
            roomName = nameIn;
        }

        public void OnNickNameChange(string nameIn) {
            PhotonNetwork.NickName = nameIn;
        }

        public void JoinLobbyOnClick() {
            if (!PhotonNetwork.InLobby) {
                PhotonNetwork.JoinLobby();
            }
        }

        private IEnumerator DisableConnectingLayer() {
            loadingLayer.transform.GetChild(0).GetComponent<Text>().text = "Connected!";
            yield return new WaitForSeconds(0.5f);
            loadingLayer.SetActive(false);
            menuLayer.SetActive(true);
        }

        private string RandomRoomName() {
            return "Room " + Random.Range(0, 1000);
        }

        private string RandomPlayerName() {
            return "Player " + Random.Range(0, 1000);
        }

        private void ActivateLoadingLayer(string text, string description = "") {
            loadingLayer.transform.GetChild(0).GetComponent<Text>().text = text;
            loadingLayer.transform.GetChild(1).GetComponent<Text>().text = description;
            loadingLayer.SetActive(true);
            menuLayer.SetActive(false);
        }
    }
}