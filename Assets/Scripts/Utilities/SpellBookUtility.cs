﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SpellBookUtility {
    public static Ability GetAbility(List<Ability> _spellBook, KeyCode _key) {
        return _spellBook.Find(ability => ability.hotkey == Constants.HotKeyMap[_key]);
    }
    public static Ability GetAbility(List<Ability> _spellBook, SlotKey _key) {
        return _spellBook.Find(ability => ability.hotkey == _key);
    }
}

