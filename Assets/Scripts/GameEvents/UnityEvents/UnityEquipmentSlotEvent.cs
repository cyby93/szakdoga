﻿using System;
using UnityEngine.Events;

[Serializable] public class UnityEquipmentSlotEvent : UnityEvent<EquipmentSlot> { }

