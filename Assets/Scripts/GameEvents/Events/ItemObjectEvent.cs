﻿using UnityEngine;

[CreateAssetMenu(fileName = "New ItemObject Event", menuName = "Game Events/ItemObject Event")]
public class ItemObjectEvent : BaseGameEvent<ItemObject> { }
