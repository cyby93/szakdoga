﻿using UnityEngine;

[CreateAssetMenu(fileName = "New InventoryItem Event", menuName = "Game Events/InventoryItem Event")]
public class InventoryItemEvent : BaseGameEvent<InventoryItem> { }
