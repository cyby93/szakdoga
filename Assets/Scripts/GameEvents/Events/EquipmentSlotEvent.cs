﻿using UnityEngine;

[CreateAssetMenu(fileName = "New EquipmentSlot Event", menuName = "Game Events/EquipmentSlot Event")]
public class EquipmentSlotEvent : BaseGameEvent<EquipmentSlot> { }
