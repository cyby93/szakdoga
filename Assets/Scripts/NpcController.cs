﻿using System.Collections;
using Photon.Pun;
using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public class NpcController : MonoBehaviour {
    private Unit unit;
    private UnitActionExecuter actions;

    private void Awake() {
        unit = GetComponent<Unit>();
        actions = GetComponent<UnitActionExecuter>();
    }

    private void Start() {
        if (PhotonNetwork.IsMasterClient)

            InvokeRepeating("SearchForTarget", 0, 1f);
    }
    
    private void SearchForTarget() {
        if (IsAnyPlayerNearby()) {
            unit.Target = FindClosestPlayer();
        }
        else {
            SearchForStructure();
        }

        actions.SetTarget(unit.Target);
    }
    
    private void SearchForStructure() {
        GameObject structure = GameObject.FindGameObjectWithTag("Structure");
        if (structure) unit.Target = structure;
    }

    private bool IsAnyPlayerNearby() {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("Player");
        float distance = 15;

        foreach (GameObject go in gos) {
            RaycastHit hit;
            Ray ray = new Ray(transform.position, go.transform.position - transform.position);
            Physics.Raycast(ray, out hit);
            if (hit.transform.gameObject == go) {
                Debug.DrawLine(transform.position, go.transform.position, Color.magenta, 1f);
                if (hit.distance < distance) return true;
            }
            else {
                Debug.DrawLine(transform.position, go.transform.position, Color.yellow, 1f);
            }
        }

        return false;
    }

    private GameObject FindClosestPlayer() {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("Player");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos) {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance) {
                closest = go;
                distance = curDistance;
            }
        }

        return closest;
    }
}