﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData 
{
    public int UniqueID { get; set; }
    public string Name { set; get; }
    public bool Ready { set; get; }

    public PlayerData() {
        this.UniqueID = Random.Range(1000, 10000);
    }
        public PlayerData(string _name, int _uid) {
        this.Name = _name;
        this.UniqueID = _uid;
    }

}
