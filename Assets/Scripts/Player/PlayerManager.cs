﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
   private static PlayerManager _instance;

   public static PlayerManager Instance {
       get {
           if(_instance == null) {
               GameObject go = new GameObject("PlayerManager");
               go.AddComponent<PlayerManager>();
           }
           return _instance;
       }
   }

    public int score = 0;

   private void Awake() {
       _instance = this;
   }
   
}
