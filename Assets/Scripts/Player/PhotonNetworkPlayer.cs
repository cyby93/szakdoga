﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Random = UnityEngine.Random;

public class PhotonNetworkPlayer : MonoBehaviourPunCallbacks {
    public GameObject SelectedUnitPrefab;
    private GameObject myPlayerUnit;
    private int sceneIndex;
    private SelectableCharacters data;

    private PhotonView _photonView;

    private void Awake() {
        DontDestroyOnLoad(gameObject);
        gameObject.name = "LocalPlayerObject";
    }

    public override void OnEnable() {
        base.OnEnable();
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        if (scene.buildIndex == 2) {
            SetGameLoadedProperty(true);
        }
    }

    void Start() {
        _photonView = GetComponent<PhotonView>();

        if (!_photonView.IsMine) return;
        PauseMenu.IsOn = false;
        SetGameLoadedProperty(false);
    }

    private void SetGameLoadedProperty(bool value) {
        Hashtable properties = PhotonNetwork.LocalPlayer.CustomProperties;
        properties[Constants.PlayerProperty.LoadingFinished] = value;
        PhotonNetwork.LocalPlayer.SetCustomProperties(properties);
    }

    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged) {
        base.OnRoomPropertiesUpdate(propertiesThatChanged);
        if (propertiesThatChanged.ContainsKey(Constants.RoomProperty.AllClientLoadingFinished) &&
            (bool) propertiesThatChanged[Constants.RoomProperty.AllClientLoadingFinished]) {
            StartCoroutine(SpawnCharacter());
        }
    }


    private IEnumerator SpawnCharacter() {
        int spawnPicker = Random.Range(0, GameSetup.singleton.playerSpawnPoints.Length);
        Transform startingLocation = GameSetup.singleton.playerSpawnPoints[spawnPicker];
        if (_photonView.IsMine && myPlayerUnit == null) {
            yield return new WaitForSeconds(1f);
            myPlayerUnit = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "Characters", SelectedUnitPrefab.name),
                                                     startingLocation.position, startingLocation.rotation, 0);
        }
    }

    public void SetSelectedCharacter(GameObject character) {
        SelectedUnitPrefab = character;
    }

    public GameObject GetMyPlayerUnit() {
        return myPlayerUnit;
    }
}