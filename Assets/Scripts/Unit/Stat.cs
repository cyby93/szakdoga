﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stat
{
    [SerializeField] private int baseValue;

    private List<float> modifiers = new List<float>();
    public int GetValue() {
        return baseValue;
    }

    public void AddModifier(int modifier){
        if(modifier != 0)
            modifiers.Add(modifier);
    }
    public void RemoveModifier(int modifier){
        if(modifier != 0)
            modifiers.Remove(modifier);
    }

    public void SetBaseValue(int _value) {
        baseValue = _value;
    }
}
