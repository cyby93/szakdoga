﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utilities {
    private static int maxLevel = 20;
    public static List<int> CreateExperienceLevels() {
        List<int> boundaries = new List<int>();
        boundaries.Add(0);
        for (int i = 1; i < maxLevel; i++) {
            boundaries.Add(CalculateXpByLevel(i));
        }
        return boundaries;
    }
    private static int CalculateXpByLevel(int level) {
        int baseXpPerLevel = 150;
        return (int) (level * baseXpPerLevel * 1.5);
    }
    
}