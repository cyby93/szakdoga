﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

[System.Serializable]

public class UnitStats : MonoBehaviour {
    private string Name { get; set; }
    public int maxHealth = 100;
    private int currentHealth { get; set; }
    public int maxEnergy = 100;
    private int currentEnergy { get; set; }
    public Stat Stamina;
    public Stat Strength;
    public Stat Spirit;
    public Stat Armor;
    public Stat AttackRange;
    public Stat AttackSpeed;
    public Stat AttackDamage;
    public Stat MovementSpeed;
    public int level;
    public int experience = 0;

    private void Awake() {
        currentHealth = maxHealth;
        currentEnergy = maxEnergy;
        level = 1;
    }

    public void AddExperience(int _xp) {
        experience += _xp;
        checkIsXpEnoughToLevelUp();
    }
    private void checkIsXpEnoughToLevelUp() {
        if (experience >= Constants.ExperienceLevels[level]) {
            level++;
            GetComponent<Unit>().onLevelUp.Invoke();
        }
    }

    public void SetHealth(int value) {
        currentHealth = value > maxHealth ? maxHealth : value;
    }
    public int GetHealth() {
        return currentHealth;
    }
    public void SetEnergy(int value) {
        currentEnergy = value > maxEnergy ? maxEnergy : value;
    }
    public int GetEnergy() {
        return currentEnergy;
    }

    [PunRPC]
    private void tempBuff() {
        currentHealth += 10;
        currentEnergy += 10;
        AddExperience(10);
    }
    void Update() {
        if (Input.GetKeyDown(KeyCode.T)) {
            if (gameObject.tag == "Player" && GetComponent<PhotonView>().IsMine) {
                GetComponent<PhotonView>().RPC("tempBuff", RpcTarget.All);
            }
        }

        if (Input.GetKeyDown(KeyCode.Z)) {
            if (gameObject.tag == "Player") {
                AttackSpeed.SetBaseValue(AttackSpeed.GetValue() + 1);
            }
        }
        if (Input.GetKeyDown(KeyCode.H)) {
            if (gameObject.tag == "Player") {
                if (AttackSpeed.GetValue() > 0)
                    AttackSpeed.SetBaseValue(AttackSpeed.GetValue() - 1);
            }
        }
    }
}