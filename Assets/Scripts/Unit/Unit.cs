﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(AbilityCastManager))]
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(StateMachine))]
public class Unit : MonoBehaviour {
    public string classType = "";
    public Action onAttack = delegate { };
    public Action onLevelUp = delegate { };

    #region Components

    private PhotonView photonView;
    private NavMeshAgent navMeshAgent;
    private NavMeshObstacle navMeshObstacle;
    private StateMachine stateMachine;
    [SerializeField] private GameObject model;
    public Animator AnimController { get; private set; }

    #endregion

    public GameObject Target { get; set; }
    public Vector3? Destination { get; set; }
    private UnitActionExecuter actions;
    private AbilityCastManager abilityCastManager;
    [SerializeField] public List<Ability> spellBook;
    [SerializeField] private GameObject levelupParticle;
    [SerializeField] private IntEvent provideExperience;
    [SerializeField] private IntEvent provideGold;
    public UnitStats UnitStats { get; private set; }

    public bool IsDead { get; private set; }

    void Awake() {
        model = Instatiaters.UnitModelInstantiater.InstatiateModel(gameObject, model);
        AnimController = model.GetComponent<Animator>();
        abilityCastManager = GetComponent<AbilityCastManager>();
        UnitStats = GetComponent<UnitStats>();
        stateMachine = GetComponent<StateMachine>();
        actions = GetComponent<UnitActionExecuter>();
        photonView = GetComponent<PhotonView>();
        InitializeStateMachine();
        InitializeAgent();
    }

    void Start() {
        StartCoroutine(UnitRegeneration());
        onLevelUp += triggerLevelUpEffect;
    }
    public NavMeshAgent GetNavMeshAgent() {
        return navMeshAgent;
    }

    public AbilityCastManager GetAbilityCastManager() {
        return abilityCastManager;
    }

    public bool IsTargetInRange() {
        Vector3 alteredStartPos = new Vector3(transform.position.x, 1, transform.position.z);
        Vector3 alteredEndPos = new Vector3(Target.transform.position.x, 1, Target.transform.position.z);
        RaycastHit hit;
        Ray ray = new Ray(alteredStartPos, alteredEndPos - alteredStartPos);
        if (Physics.Raycast(ray, out hit)) {

            if (hit.transform.gameObject != Target) return false;
            RaycastHit anotherHit;
            Ray ray2 = new Ray(hit.point, alteredStartPos - hit.point);

            if (Physics.Raycast(ray2, out anotherHit)) {
                Debug.DrawLine(hit.point, anotherHit.point, Color.red);
                if (anotherHit.distance <= 0.5f) return true;
            }
            else {
                Debug.DrawLine(alteredStartPos, hit.point, Color.green);
            }
        }

        return false;
    }

    // public void Die() {
    //     photonView.RPC("KillUnit", RpcTarget.All);
    // }

    [PunRPC]
    private void KillUnit() {
        StartCoroutine(SetUnitDead());
    }

    public IEnumerator SetUnitDead() {
        IsDead = true;
        ProvideXpForNearbyPlayers();
        gameObject.tag += "-Dead";
        navMeshAgent.enabled = false;
        navMeshObstacle.enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<CapsuleCollider>().enabled = false;
        provideExperience.Raise(UnitStats.experience);
        provideGold.Raise(UnitStats.level * 2);
        yield return new WaitForSeconds(8f);

        if (photonView.IsMine)
            PhotonNetwork.Destroy(gameObject);
    }


    public void TakeDamage(GameObject attacker, int damage) {
        if (!PhotonNetwork.IsMasterClient) return;

        damage -= UnitStats.Armor.GetValue();
        damage = Mathf.Clamp(damage, 0, int.MaxValue);
        photonView.RPC("RPC_TakeDamage", RpcTarget.All, damage);

        if (UnitStats.GetHealth() <= 0 && !IsDead) {
            photonView.RPC("KillUnit", RpcTarget.All);
        }
        else {
            if (attacker != null && isUnitIdle()) actions.SetTarget(attacker);
        }
    }
    
    [PunRPC]
    private void RPC_TakeDamage(int damage) {
        UnitStats.SetHealth(UnitStats.GetHealth() - damage);
    }
    
    public void HealDamage(int amount) {
        if (!PhotonNetwork.IsMasterClient) return;
        photonView.RPC("RPC_HealDamage", RpcTarget.All, amount);
    }

   

    [PunRPC]
    private void RPC_HealDamage(int amount) {
        UnitStats.SetHealth(UnitStats.GetHealth() + amount);
    }

    public GameObject GetUnitModel() {
        return model;
    }

    public void SetUnitModel(GameObject newModel) {
        model = newModel;
    }

    public bool IsUnitCasting() {
        return abilityCastManager != null && abilityCastManager.IsCasting();
    }

    private void InitializeStateMachine() {
        stateMachine = GetComponent<StateMachine>();
        var states = new Dictionary<Type, BaseState>() {
            {typeof(IdleState), new IdleState(this)}, {typeof(MoveState), new MoveState(this)},
            {typeof(ChaseState), new ChaseState(this)}, {typeof(AttackState), new AttackState(this)},
            {typeof(DeadState), new DeadState(this)}, {typeof(ReadyCastState), new ReadyCastState(this)}
        };
        stateMachine.SetStates(states);
    }

    private void InitializeAgent() {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.speed = UnitStats.MovementSpeed.GetValue();
        navMeshObstacle = GetComponent<NavMeshObstacle>();
        navMeshAgent.enabled = false;
    }

    private void Regenerate() {
        UnitStats.SetHealth(UnitStats.GetHealth() + (UnitStats.Strength.GetValue() / 10));
        UnitStats.SetEnergy(UnitStats.GetEnergy() + (UnitStats.Spirit.GetValue() / 10));
    }

    private IEnumerator UnitRegeneration() {
        while (!IsDead) {
            yield return new WaitForSeconds(1f);
            Regenerate();
        }
    }

    private bool isUnitIdle() {
        return Target == null && stateMachine.CurrentState.GetType() == typeof(IdleState);
    }

    private void triggerLevelUpEffect() {
        StartCoroutine(LevelUpEffect());
    }

    private IEnumerator LevelUpEffect() {
        GameObject lupGo = Instantiate(levelupParticle, transform.position, transform.rotation);
        lupGo.transform.SetParent(transform);
        yield return new WaitForSeconds(4f);
        DestroyImmediate(lupGo);
    }

    private void ProvideXpForNearbyPlayers() {
        if (CompareTag("Enemy")) {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject go in players) {
                go.GetComponent<UnitStats>().AddExperience(UnitStats.experience);
            }
        }
    }

    public void SetNavMeshAgentEnabled(bool value) {
        navMeshAgent.enabled = value;
        navMeshObstacle.enabled = !value;
    }
}