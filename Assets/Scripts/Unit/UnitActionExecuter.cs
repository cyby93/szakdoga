﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(AbilityCastManager))]
[RequireComponent(typeof(Unit))]
public class UnitActionExecuter : MonoBehaviour {
    private Unit unit;
    private UnitStats unitStats;
    private NetworkInstanceId ownNetId;
    private Unit targetUnit;
    private float attackCooldown;
    private Ability selectedAbility;
    private AbilityCastManager abilityCastManager;
    private PhotonView photonView;

    private void Awake() {
        abilityCastManager = GetComponent<AbilityCastManager>();
        unit = GetComponent<Unit>();
        unitStats = GetComponent<UnitStats>();
        unit.onAttack += AutoAttack;
        photonView = GetComponent<PhotonView>();
    }

    void Update() {
        if (!PhotonNetwork.IsMasterClient) return;

        if (attackCooldown > 0) attackCooldown -= Time.deltaTime;
        if (targetUnit && targetUnit.IsDead)
            photonView.RPC("RPC_ClearTarget", RpcTarget.All);
    }

    #region MoveTo

    public void MoveTo(Vector3 point) {
        if (abilityCastManager.HasSelectedAbility()) {
            abilityCastManager.CancelCasting();
        }
        else {
            photonView.RPC("RPC_SetDestination", RpcTarget.All, point);
        }
    }
    [PunRPC]
    private void RPC_SetDestination(Vector3 point) {
        unit.Destination = point;
        unit.Target = null;
        abilityCastManager.CancelCasting();
    }

    #endregion

    #region Casting

    public void StartCastingAbility(Vector3 targetPosition) {
        photonView.RPC("RPC_StartCastingAbility", RpcTarget.All, targetPosition);
    }

    [PunRPC]
    private void RPC_StartCastingAbility(Vector3 targetPosition) {
        abilityCastManager.StartCastingAbility(targetPosition);
    }

    #endregion


    #region SetTarget

    public void SetTarget(GameObject target)
    {
        if (!target.GetComponent<PhotonView>()) return;
        int targetId = target.GetComponent<PhotonView>().ViewID;
        photonView.RPC("RPC_SetTarget", RpcTarget.All, targetId);
    }

    [PunRPC]
    private void RPC_SetTarget(int targetId) {
        unit.Target = PhotonView.Find(targetId).gameObject;
    }

    [PunRPC]
    private void RPC_ClearTarget() {
        unit.Target = null;
        targetUnit = null;
    }

    #endregion

    #region AutoAttack

    private void AutoAttack() {
        unit.AnimController.SetFloat("AttackSpeedParameter", unitStats.AttackSpeed.GetValue());

        if (attackCooldown <= 0f) {
            StartCoroutine(DoDamage(unit.Target));
            attackCooldown = Constants.BaseAnimationDuration / unitStats.AttackSpeed.GetValue();
        }
    }

    IEnumerator DoDamage(GameObject target) {
        float halfDurationOfAnimation = 0.5f / unitStats.AttackSpeed.GetValue();
        if (!isStructure(target)) {
            targetUnit = unit.Target.GetComponent<Unit>();
            yield return new WaitForSeconds(halfDurationOfAnimation);
            if (unit.Target && targetUnit == target.GetComponent<Unit>() &&
                GetComponent<StateMachine>().CurrentState.GetType() == typeof(AttackState)) {
                target.GetComponent<Unit>().TakeDamage(gameObject, unitStats.AttackDamage.GetValue());
            }
        }
        else {
            yield return new WaitForSeconds(halfDurationOfAnimation);
            if (PhotonNetwork.IsMasterClient && unit.Target &&
                GetComponent<StateMachine>().CurrentState.GetType() == typeof(AttackState))
                photonView.RPC("DealDamageToStructure", RpcTarget.All);
        }
    }

    [PunRPC]
    private void DealDamageToStructure()
    {
        if (!unit.Target) return;
        UnitStats targetstat = unit.Target.GetComponent<UnitStats>();
        if (!targetstat) return;
        targetstat.SetHealth(targetstat.GetHealth() - unitStats.AttackDamage.GetValue());
    }

    #endregion

    #region Abilities

    public void selectAbility(KeyCode _key) {
        abilityCastManager.SelectAbility(Constants.HotKeyMap[_key]);
    }

    private bool isStructure(GameObject go) {
        return go.CompareTag("Structure");
    }

    #endregion
}