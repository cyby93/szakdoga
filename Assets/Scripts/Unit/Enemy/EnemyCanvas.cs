﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCanvas : MonoBehaviour
{
    void Update()
    {
        if(transform.rotation != Camera.main.transform.rotation) transform.rotation = Camera.main.transform.rotation;
        if(gameObject.transform.parent.GetComponent<Unit>().UnitStats.GetHealth() <= 0) {
            DestroyImmediate(gameObject);
        }
    }
}
