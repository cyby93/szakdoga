﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EnemyHealthbar : MonoBehaviour
{

    GameObject unitGameObject;
    Unit unit;
    private Image Healthbar;

    private int MaxHealth, MaxEnergy;
    private void Awake() {
        unitGameObject = gameObject.transform.parent.gameObject.transform.parent.gameObject; 
        unit = unitGameObject.GetComponent<Unit>();
        Healthbar = this.gameObject.transform.GetChild(0).GetComponent<Image>();
    }
    void Update()
    {   
        Healthbar.fillAmount = ((float)unit.UnitStats.GetHealth() / (float)unit.UnitStats.maxHealth);
    }
}
