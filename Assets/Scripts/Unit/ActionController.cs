﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class ActionController : MonoBehaviour {
    private PhotonView photonView;

    void Start() {
        photonView = GetComponent<PhotonView>();
    }

    public void WannaSetTarget(GameObject target) {
        int targetId = target.GetComponent<PhotonView>().ViewID;

        if (PhotonNetwork.IsMasterClient)
            photonView.RPC("RPC_SetTarget", RpcTarget.All, targetId);
        else
            TellMasterToRPC();
    }

    private void TellMasterToRPC() {
        
    }
}