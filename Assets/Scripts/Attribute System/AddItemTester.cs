﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddItemTester : MonoBehaviour {
    
    [SerializeField] private ArmorItem item1 = null;
    
    
    [ContextMenu("test equip item")]
    public void EquipItem() {
        ItemEquipManager.Instance.EquipItem(item1);
    }
    [ContextMenu("test unequip item")]
    public void UnEquipItem() {
        ItemEquipManager.Instance.UnEquipItem(item1);
    }
    [ContextMenu("test equip item3")]
    public void EquipItem2() {
        ItemEquipManager.Instance.EquipItem(item1);
    }
    [ContextMenu("test unequip item3")]
    public void UnEquipItem2() {
        ItemEquipManager.Instance.UnEquipItem(item1);
    }

}