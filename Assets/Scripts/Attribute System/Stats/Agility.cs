﻿
using System;
using UnityEngine;

[Serializable]
public class Agility : BaseAttribute
{
    public Agility(int _baseValue) : base(_baseValue) {
    }
}
