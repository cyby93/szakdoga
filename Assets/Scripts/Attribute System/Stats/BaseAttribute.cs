﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BaseAttribute {
    public string Description { get; protected set; }
    public Sprite Icon { get; protected set; }
    [SerializeField] private float baseValue;
    [SerializeField] public float finalValue;
    public event Action OnModifiersChange;
    
    protected readonly List<StatModifier> Modifiers;
    
    protected BaseAttribute(int _baseValue) {
        baseValue = _baseValue;
        finalValue = baseValue;
        Modifiers = new List<StatModifier>();
        OnModifiersChange += UpdateCalculatedValue;
    }
    
    public float BaseValue => baseValue;
    
    public void AddModifier(StatModifier modifier) {
        Modifiers.Add(modifier);
        if (OnModifiersChange != null) OnModifiersChange.Invoke();
    }
    
    public void RemoveModifier(StatModifier modifier) {
        Modifiers.Remove(modifier);
        if (OnModifiersChange != null) OnModifiersChange.Invoke();
    }

    public float CalculatedValue() {
        var calculatedValue = baseValue;
        Modifiers.ForEach(modifier => calculatedValue += modifier.Value);
        return calculatedValue;
    }

    protected virtual void UpdateCalculatedValue() {
        finalValue = CalculatedValue();
    }
}
