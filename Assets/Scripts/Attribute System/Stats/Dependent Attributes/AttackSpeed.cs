﻿using System;
using UnityEngine;

[Serializable]
public class AttackSpeed : DependentAttribute
{
    public AttackSpeed(int baseValue, BaseAttribute[] attributes) : base(baseValue, attributes) {
    }
    public AttackSpeed(BaseAttribute[] attributes): base(1, attributes) {}

    protected override float AgilityCalculation(BaseAttribute agility) {
        return agility.CalculatedValue() * 0.02f;
    }
}
