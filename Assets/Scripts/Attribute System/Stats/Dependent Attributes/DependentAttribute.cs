﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DependentAttribute : BaseAttribute {
    protected List<BaseAttribute> Attributes { set; get; }

    protected DependentAttribute(int baseValue, BaseAttribute[] attributes) : base(baseValue) {
        Attributes = new List<BaseAttribute>();
        foreach (BaseAttribute attribute in attributes) {
            Attributes.Add(attribute);
            attribute.OnModifiersChange += UpdateCalculatedValue;
        }
    }

    protected override void UpdateCalculatedValue() {
        var calculatedValue = BaseValue;
        foreach (BaseAttribute attribute in Attributes) {
            if (attribute.GetType() == typeof(Agility)) {
                calculatedValue += AgilityCalculation(attribute);
            }

            if (attribute.GetType() == typeof(Stamina)) {
                calculatedValue += StaminaCalculation(attribute);
            }

            if (attribute.GetType() == typeof(Intellect)) {
                calculatedValue += IntellectCalculation(attribute);
            }
        }

        finalValue = calculatedValue;
    }

    protected virtual float AgilityCalculation(BaseAttribute agility) {
        return 0f;
    }

    protected virtual float StaminaCalculation(BaseAttribute stamina) {
        return 0f;
    }

    protected virtual float IntellectCalculation(BaseAttribute intellect) {
        return 0f;
    }
}