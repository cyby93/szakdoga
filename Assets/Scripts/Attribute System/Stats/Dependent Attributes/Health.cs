﻿using System;
using UnityEngine;

[Serializable]
public class Health : DependentAttribute
{
    public Health(int baseValue, BaseAttribute[] attributes) : base(baseValue, attributes) { }

    protected override float StaminaCalculation(BaseAttribute stamina) {
        return stamina.CalculatedValue() * 10;
    }
}
