﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour {
    [Header("Base Attributes")]
    [SerializeField] private Stamina stamina;
    [SerializeField] private Intellect intellect;
    [SerializeField] private Agility agility;

    [Header("Computed Attributes")] [SerializeField]
    private Health health;

    [SerializeField] private AttackSpeed attackSpeed;

    public int Level { private set; get; }
    public int Experience { set; get; }

    private void OnEnable() {
        stamina = new Stamina(10);
        intellect = new Intellect(15);
        agility = new Agility(10);
        attackSpeed = new AttackSpeed(new BaseAttribute[] {agility});
        health = new Health(0, new BaseAttribute[] {stamina});

        ItemEquipManager.Instance.OnItemEquip += AddModifiersToStatData;
        ItemEquipManager.Instance.OnItemUnequip += RemoveModifiersFromStat;
    }
    
    private void OnDisable() {
        ItemEquipManager.Instance.OnItemEquip -= AddModifiersToStatData;
        ItemEquipManager.Instance.OnItemUnequip -= RemoveModifiersFromStat;
    }
    
    private void AddModifiersToStatData(ArmorItem item) {
        ModificationDataBlock modificators = item.Modifiers;
        stamina.AddModifier(modificators.stamina);
        agility.AddModifier(modificators.agility);
        intellect.AddModifier(modificators.intellect);
    }

    private void RemoveModifiersFromStat(ArmorItem item) {
        ModificationDataBlock modificators = item.Modifiers;
        stamina.RemoveModifier(modificators.stamina);
        agility.RemoveModifier(modificators.agility);
        intellect.RemoveModifier(modificators.intellect);

    }

}