﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class StatModifier {
    [SerializeField] private int value;
    public StatModifier(int modifier) {
        value = modifier;
    }

    public int Value => value;
}
