﻿using UnityEngine;

public class CameraController : MonoBehaviour {
    public float panSpeed = 10f;
    public float panBorderThickness = 10f;

    public Vector2 panMaxLimit;
    private GameObject playerCharacter;

    public float rotationSpeed = 20f;
    public float scrollSpeed = 30f;
    private float minCameraHeight = 20f;
    private float maxCameraHeight = 60f;

    private bool followPlayer;
    private bool lockedCamera = true;

    private Vector3 nextCameraPosition;

    void Start() {
        SetStartPosition();
        InitCamera();
    }

    void Update() {
        if (isCameraInitialized()) {
            handleCameraLock();
            moveCameraToCharacterOnSpace();

            if (lockedCamera || followPlayer) {
                MoveCameraToPlayer();
            }
            else {
                MoveCamera();
            }

            HandleScrolling();
            MoveCameraSmoothly();
        }
        else {
            InitCamera();
        }
    }

    private void SetStartPosition() {
        Vector3 position = new Vector3(0, 20, -20);
        transform.position = position;
        transform.LookAt(Vector3.zero);
    }
    private void InitCamera() {
        if (isLocalPlayerCharacterAlreadyInitialized()) {
            playerCharacter = Constants.LocalPlayerObject.GetComponent<PhotonNetworkPlayer>()
                .GetMyPlayerUnit();
            transform.position = playerCharacter.transform.position - new Vector3(0, 0, 20);
            transform.position += new Vector3(0, 20, 0);
            transform.LookAt(playerCharacter.transform.position);
            nextCameraPosition = transform.position;
        }
    }

    private void MoveCameraSmoothly() {
        if (transform.position != nextCameraPosition) {
            transform.position = Vector3.MoveTowards(transform.position, nextCameraPosition, 0.5f);
        }
    }

    private void handleCameraLock() {
        if (Input.GetKeyDown("l")) {
            lockedCamera = !lockedCamera;
        }
    }

    private void moveCameraToCharacterOnSpace() {
        if (Input.GetKeyDown("space")) {
            followPlayer = true;
            lockedCamera = false;
        }
    }

    private void MoveCameraToPlayer() {
        Vector3 playerpos = playerCharacter.transform.position;
        playerpos.y = transform.position.y;
        playerpos.z -= transform.position.y;
        transform.position = Vector3.MoveTowards(transform.position, playerpos, 5f);
        if (Vector3.Distance(transform.position, playerpos) <= 1) {
            followPlayer = false;
        }
    }

    private void MoveCamera() {
        Vector3 pos = transform.position;

        if (Input.GetKey("up") || Input.mousePosition.y >= Screen.height - panBorderThickness) {
            pos.z += panSpeed * Time.deltaTime;
        }

        if (Input.GetKey("down") || Input.mousePosition.y <= panBorderThickness) {
            pos.z -= panSpeed * Time.deltaTime;
        }

        if (Input.GetKey("right") || Input.mousePosition.x >= Screen.width - panBorderThickness) {
            pos.x += panSpeed * Time.deltaTime;
        }

        if (Input.GetKey("left") || Input.mousePosition.x <= panBorderThickness) {
            pos.x -= panSpeed * Time.deltaTime;
        }

        pos.x = Mathf.Clamp(pos.x, -panMaxLimit.x, panMaxLimit.x);
        pos.z = Mathf.Clamp(pos.z, -panMaxLimit.y, panMaxLimit.y);
        pos.y = Mathf.Clamp(pos.y, minCameraHeight, maxCameraHeight);
        nextCameraPosition = pos;
    }

    private void HandleScrolling() {
        if (Input.GetAxis("Mouse ScrollWheel") == 0) return;
        Vector3 pos = transform.position;
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        pos.y -= (scroll * scrollSpeed * 100f * Time.deltaTime);
        pos.y = Mathf.Clamp(pos.y, minCameraHeight, maxCameraHeight);
        nextCameraPosition = pos;
    }

    private bool isCameraInitialized() {
        return !!playerCharacter;
    }

    private bool isLocalPlayerCharacterAlreadyInitialized() {
        return Constants.LocalPlayerObject &&
               Constants.LocalPlayerObject.GetComponent<PhotonNetworkPlayer>().GetMyPlayerUnit() != null;
    }
}