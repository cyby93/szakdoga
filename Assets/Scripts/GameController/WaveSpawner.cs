﻿using System;
using System.Collections;
using System.IO;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class WaveSpawner : MonoBehaviour {
    private PhotonView photonView;

    private enum SpawnState {
        Spawning,
        Waiting,
        Counting,
        Stopped
    }

    [Serializable]
    public class Wave {
        public string name;
        public GameObject enemy;
        public int count;
        public float rate;
    }

    public Wave[] waves;
    [SerializeField] private int manualSpawningNumber = 0;

    [SerializeField] private bool enableSpawning;
    public float timeBetweenWaves;
    public float waveCountDown;
    public float firstWaveDelay;
    private float searchCountdown = 1f;
    private SpawnState state = SpawnState.Counting;
    private int nextWave;
    private bool isCoroutineRunning = false;


    void Start() {
        photonView = GetComponent<PhotonView>();

        if (!PhotonNetwork.IsMasterClient) return;
        SetWaveProperties();
    }

    void Update() {
        if (!PhotonNetwork.IsMasterClient || !enableSpawning) return;
        
        if (state == SpawnState.Spawning && !isCoroutineRunning) 
            StartCoroutine(SpawnWave(waves[nextWave]));
        
        if (state == SpawnState.Counting) 
            CountdownUntilNextWaveSpawn();

        if (state == SpawnState.Waiting) 
            WaitingForWaveClear();
        
        if (state == SpawnState.Stopped) 
            enableSpawning = false;
    }

    private void WaitingForWaveClear() {
        if (nextWave + 1 == waves.Length) {
            state = SpawnState.Stopped;
        }

        if (!EnemyIsAlive()) {
            state = SpawnState.Counting;
            nextWave++;
            UpdateWaveInfo(nextWave + 1);
        }
    }

    private void CountdownUntilNextWaveSpawn() {
        if (waveCountDown <= 0) {
            state = SpawnState.Spawning;
        }
        else {
            waveCountDown -= Time.deltaTime;
        }
    }


    private void UpdateWaveInfo(int currentWave) {
        ExitGames.Client.Photon.Hashtable properties = PhotonNetwork.CurrentRoom.CustomProperties;
        properties[Constants.RoomProperty.CurrentWaveNumber] = currentWave;
        PhotonNetwork.CurrentRoom.SetCustomProperties(properties);
    }


    private void SetWaveProperties() {
        ExitGames.Client.Photon.Hashtable properties = PhotonNetwork.CurrentRoom.CustomProperties;
        properties.Add(Constants.RoomProperty.MaxWaveNumber, waves.Length);
        properties.Add(Constants.RoomProperty.TimeBetweenWaves, timeBetweenWaves);
        properties.Add(Constants.RoomProperty.CurrentWaveNumber, 1);
        PhotonNetwork.CurrentRoom.SetCustomProperties(properties);
    }

    private bool EnemyIsAlive() {
        searchCountdown -= Time.deltaTime;
        if (searchCountdown <= 0f) {
            searchCountdown = 1f;
            if (GameObject.FindGameObjectWithTag("Enemy") == null) {
                return false;
            }
        }

        return true;
    }

    IEnumerator SpawnWave(Wave wave) {
        print("Starting coroutine shit");
        isCoroutineRunning = true;
        if (nextWave == 0) {
            yield return new WaitForSeconds(firstWaveDelay);
        }
        for (int i = 0; i < wave.count; i++) {
            SpawnEnemy(wave.enemy.gameObject.name);
            yield return new WaitForSeconds(wave.rate);
        }

        state = SpawnState.Waiting;
        waveCountDown = timeBetweenWaves;
        isCoroutineRunning = false;
    }

    private void SpawnEnemy(string enemy) {
        float randomFloat = Random.Range(-3, 3);
        Vector3 pos = new Vector3(randomFloat, 0, randomFloat);
        foreach (var spawnPoint in GameSetup.singleton.enemySpawnPoints) {
            string prefabName = Path.Combine("Prefabs", "EnemyUnits", enemy);
            PhotonNetwork.Instantiate(prefabName,
                                      spawnPoint.position - pos,
                                      Quaternion.identity, 0);
        }
    }
}