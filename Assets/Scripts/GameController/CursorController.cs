﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;

public class CursorController : MonoBehaviour {
    [SerializeField] Texture2D normalCursor;
    [SerializeField] Texture2D attackCursor;
    [SerializeField] Texture2D activeCursor;

    private Dictionary<CursorType, Texture2D> cursorList;
    private CursorType currentCursorType = CursorType.NORMAL;
    public static CursorController Instance { get; private set; }
    private void Awake() {
        Instance = this;
        initializeCursorList();
    }
    private void Start() {
        SetNormalCursor();
    }

    void Update() {
        if (!isActiveCursor(CursorType.CAST)) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo)) {
                if (isEnemyHit(hitInfo)) {
                    SetAttackCursor();
                } else {
                    SetNormalCursor();
                }
            }
        }

    }
    public bool isActiveCursor(CursorType _cursorType) {
        return currentCursorType == _cursorType;
    }
    public void SetAttackCursor() {
        setCursorType(CursorType.ATTACK);
    }
    public void SetNormalCursor() {
        setCursorType(CursorType.NORMAL);
    }
    public void SetActiveCursor() {
        setCursorType(CursorType.CAST);
    }
    private void setCursorType(CursorType _cursorType) {
        if (cursorList.TryGetValue(_cursorType, out Texture2D _texture)) {
            Cursor.SetCursor(_texture, Vector2.zero, CursorMode.Auto);
            currentCursorType = _cursorType;
        }
    }
    private void initializeCursorList() {
        cursorList = new Dictionary<CursorType, Texture2D>();
        cursorList.Add(CursorType.NORMAL, normalCursor);
        cursorList.Add(CursorType.ATTACK, attackCursor);
        cursorList.Add(CursorType.CAST, activeCursor);
    }

    private bool isEnemyHit(RaycastHit _hitInfo) {
        return _hitInfo.transform.gameObject.tag == "Enemy" || _hitInfo.collider.gameObject.tag == "Enemy";
    }
}
public enum CursorType {
    NORMAL,
    ATTACK,
    CAST
}