﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class UnitMovement : MonoBehaviour
{
    [SerializeField] private NavMeshAgent agent;
 
    private Vector3 destinationPoint;
    void Update()
    {
       
    }

    public void Move(Vector3 _destinationPoint){
        agent.SetDestination(_destinationPoint);
    }
}
