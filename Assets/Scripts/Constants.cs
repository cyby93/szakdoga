﻿using System.Collections;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class Constants {
    public const float BaseAnimationDuration = 1;

    public static readonly List<KeyCode> AbilityKeyCodes = new List<KeyCode>() {
        KeyCode.Q, KeyCode.W, KeyCode.E, KeyCode.R, KeyCode.A, KeyCode.S, KeyCode.D, KeyCode.F
    };

    public static readonly Dictionary<KeyCode, SlotKey> HotKeyMap = new Dictionary<KeyCode, SlotKey>() {
        {KeyCode.Q, SlotKey.Q}, {KeyCode.W, SlotKey.W}, {KeyCode.E, SlotKey.E}, {KeyCode.R, SlotKey.R},
        {KeyCode.A, SlotKey.A}, {KeyCode.S, SlotKey.S}, {KeyCode.D, SlotKey.D}, {KeyCode.F, SlotKey.F}
    };

    public  static readonly List<int> ExperienceLevels = Utilities.CreateExperienceLevels();

    public enum UnitState {
        Idle,
        Moving,
        Attacking,
        Casting,
        FinishCasting,
        Dead,
    }
    public class RoomProperty {
        public const string CurrentWaveNumber = "CurrentWaveNumber";
        public const string MaxWaveNumber = "MaxWaveNumber";
        public const string TimeBetweenWaves = "TimeBetweenWaves";
        public const string AllClientLoadingFinished = "LoadingFinished";
        public const string IsCountdownOn = "IsCountdownOn";
        public const string GameStarted = "GameStarted";
    }

    public class PlayerProperty {
        public const string IsReady = "IsReady";
        public const string LoadingFinished = "LoadingFinished";
    }
    public const string LocalPlayer = "LocalPlayerObject";
    public static readonly GameObject LocalPlayerObject = GameObject.Find(LocalPlayer);
}

