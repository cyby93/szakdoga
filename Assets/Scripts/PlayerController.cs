﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Photon.Pun;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    private UnitActionExecuter actions;
    private Camera cam;
    private PhotonView photonView;
    private GameObject navigationMark;
    private GameObject targetMark;
    private Coroutine markerDisableCoroutine;

    private GameObject navigationMarkPrefab;

    private GameObject targetMarkPrefab;

    void Start() {
        cam = Camera.main;
        photonView = GetComponent<PhotonView>();
        actions = GetComponent<UnitActionExecuter>();
        navigationMarkPrefab = Resources.Load<GameObject>(Path.Combine("Prefabs", "Markers", "NavigationMark"));
        targetMarkPrefab = Resources.Load<GameObject>(Path.Combine("Prefabs", "Markers", "TargetMark"));
        navigationMark = Instantiate(navigationMarkPrefab, transform.position, Quaternion.identity);
        targetMark = Instantiate(targetMarkPrefab);
        navigationMark.SetActive(false);
        targetMark.SetActive(false);
        markerDisableCoroutine = StartCoroutine(HideMarks());
    }

    void Update() {
        if (!photonView.IsMine) return;

        HandleRightClick();
        HandleAbilityHotkeys();
    }


    private void HandleAbilityHotkeys() {
        if (IsAbilityHotKeyPressed()) {
            foreach (var vKey in Constants.AbilityKeyCodes) {
                if (Input.GetKey(vKey)) {
                    actions.selectAbility(vKey);
                }
            }
        }
    }

    private void HandleRightClick() {
        if (Input.GetMouseButtonDown(1)) {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            int groundLayerMask = 1 << LayerMask.NameToLayer("Ground");
            int enemyLayerMask = 1 << LayerMask.NameToLayer("Enemy");
            int combinedLayerMask = groundLayerMask | enemyLayerMask;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, combinedLayerMask)) {
                if (isEnemy(hit)) {
                    actions.SetTarget(hit.transform.gameObject);
                    SetTargetMark(hit.transform.gameObject);
                }
                else {
                    actions.MoveTo(hit.point);
                    SetNavigationMark(hit.point);
                }
            }
        }
    }

    private IEnumerator HideMarks() {
        yield return new WaitForSeconds(1f);
        if (targetMark) targetMark.SetActive(false);
        if (navigationMark) navigationMark.SetActive(false);
    }

    private void SetTargetMark(GameObject target) {
        if (markerDisableCoroutine != null) StopCoroutine(markerDisableCoroutine);
        if (!targetMark)
            targetMark = Instantiate(targetMarkPrefab);
        targetMark.transform.SetParent(target.transform);
        targetMark.transform.position = Vector3.zero;
        targetMark.SetActive(true);
        markerDisableCoroutine = StartCoroutine(HideMarks());
    }

    private void SetNavigationMark(Vector3 point) {
        if (markerDisableCoroutine != null) StopCoroutine(markerDisableCoroutine);
        navigationMark.transform.position = point;
        navigationMark.SetActive(true);
        markerDisableCoroutine = StartCoroutine(HideMarks());
    }

    private bool isEnemy(RaycastHit hit) {
        return hit.transform.gameObject.CompareTag("Enemy");
    }

    private static bool IsAbilityHotKeyPressed() {
        return Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.E) ||
               Input.GetKeyDown(KeyCode.R);
    }
}