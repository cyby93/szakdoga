﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMarkAdjuster : MonoBehaviour {
    [SerializeField] private GameObject markArrow;
    [SerializeField] private GameObject markSpinner;
    
    void Update() {
        RaycastHit hit;
        Vector3 from = transform.position;
        from.y = 20;
        
        Ray ray = new Ray(from, Vector3.down);
        if (Physics.Raycast(ray, out hit)) {
            Debug.DrawLine(from, hit.point, Color.red);
             calculateArrowPosition(hit.point);
             calculateSpinnerRadius(hit);
        }
    }

    private void calculateArrowPosition(Vector3 point) {
        markArrow.transform.position = point;
    }
    
    private void calculateSpinnerRadius(RaycastHit hit)
    {
        if (!hit.collider.gameObject.GetComponent<CapsuleCollider>()) return;
        float radius = hit.collider.gameObject.GetComponent<CapsuleCollider>().radius;
        Vector3 newScale = new Vector3(radius, 1, radius);
        markSpinner.transform.localScale = newScale;
    }
}
