﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionBorder : MonoBehaviour
{
    void Start() {
        RectTransform rect = GetComponent<RectTransform>();
        rect.anchoredPosition = new Vector2(0,0);
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);
    }
    void Update()
    {
        if(transform.parent.gameObject.GetComponent<CommandIcon>().IsActive() == true){
            GetComponent<Image>().enabled = true;
        } else {
            GetComponent<Image>().enabled = false;
        }
    }
}
