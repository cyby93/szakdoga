﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RedOverlay : MonoBehaviour
{
    private float interval = 1;
    private CommandIcon icon;
    private Image image;
    private bool animationOn;
    private float speed = 0.5f;
    void Start() {
        icon = transform.parent.gameObject.GetComponent<CommandIcon>();
        image =  GetComponent<Image>();
        RectTransform rect = GetComponent<RectTransform>();
        rect.anchoredPosition = new Vector2(0,0);
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);

    }

    void Update()
    {
        
        if(icon.IsActive()){
            animationOn = true;
        } else {
            animationOn = false;
            image.enabled = false;
        }
        if(animationOn){
            if(interval <= -speed) interval = speed;
            if(interval >= 0) image.enabled = true;
            else image.enabled = false;
            interval -= 1 * Time.deltaTime;
            
        }
    }
}
