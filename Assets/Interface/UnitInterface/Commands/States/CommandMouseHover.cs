﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CommandMouseHover : MonoBehaviour
{
    void Start() {
        RectTransform rect = GetComponent<RectTransform>();
        rect.anchoredPosition = new Vector2(0,0);
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);
    }
    // Update is called once per frame
    void Update()
    {
        if(transform.parent.gameObject.GetComponent<CommandIcon>().isHovered() == true){
            GetComponent<Image>().enabled = true;
        } else {
            GetComponent<Image>().enabled = false;
        }

    }
}
