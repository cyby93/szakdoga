﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnitDependencies;

public class CommandIcon : MonoBehaviour, IPointerClickHandler,
     IPointerExitHandler, IPointerEnterHandler
{

    private Camera cam;
    private bool isActive;
    private bool hovering;
    private GameObject hover;
    private GameObject active;
    private GameObject red;
    public GameObject hoverObject;
    public GameObject BorderOverlayObject;
    public GameObject RedOverlayObject;
    public ACTION_STATE Command;

    Image image;
    void Start()
    {
        cam = Camera.main;
        image = GetComponent<Image>();

        hover = Instantiate(hoverObject);
        hover.transform.parent = gameObject.transform;

        active = Instantiate(BorderOverlayObject);
        active.transform.parent = gameObject.transform;

    }

    // Update is called once per frame
    void Update()
    {
        if(Command == GlobalData.playerCurrentAction) {
            isActive = true;
        } else {
            isActive = false;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        // isActive = !isActive;
    }

     public void OnPointerEnter(PointerEventData eventData)
    {
        hovering = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        hovering = false;
    }

    public bool IsActive() {
        return isActive;
    }
    public bool isHovered() {
        return hovering;
    }
}
