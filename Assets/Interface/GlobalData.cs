﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnitDependencies;
using UnityEngine.UI;
public static class GlobalData
{
    public static ACTION_STATE playerCurrentAction;
    public static string NickName;
    public static bool NickNameEmpty = false;
    public static void SetNickName(Text _nickNameTextField) {
        NickName = _nickNameTextField.text;

        if(_nickNameTextField.text == null || _nickNameTextField.text == "") NickNameEmpty = true;
        else NickNameEmpty = false;
    }

    public static List<PlayerData> PlayerList = new List<PlayerData>();

    public static int onlineplayers = 0;
}
