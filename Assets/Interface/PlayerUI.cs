﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUI : MonoBehaviour {
    [SerializeField] private GameObject pauseMenu;

    [SerializeField] private GameObject inventory;

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) TogglePauseMenu();
        
        if (Input.GetKeyDown(KeyCode.B)) ToggleInventory();
    }


    private void TogglePauseMenu() {
        pauseMenu.SetActive(!pauseMenu.activeSelf);
        PauseMenu.IsOn = pauseMenu.activeSelf;
    }

    private void ToggleInventory() => inventory.SetActive(!inventory.activeSelf);
}