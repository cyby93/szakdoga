﻿using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PUN_Party : MonoBehaviourPunCallbacks, ILobbyCallbacks {
    public static PUN_Party lobby;

    public string roomName;
    public int roomSize = 12;
    public GameObject roomListingPrefab;
    public Transform roomsPanel;

    private void Awake() {
        lobby = this;
    }

    void Start() {
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster() {
        Debug.Log("Player has connected to the Photon server!");
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList) {
        base.OnRoomListUpdate(roomList);
        RemoveRoomListings();
        foreach (RoomInfo room in roomList) {
            ListRoom(room);
        }
    }

    public void RefreshRoomList() {
    }

    private void RemoveRoomListings() {
        for (int i = roomsPanel.childCount - 1; i >= 0; i--) {
            Destroy(roomsPanel.GetChild(i).gameObject);
        }
    }

    private void ListRoom(RoomInfo room) {
        if (room.IsOpen && room.IsVisible) {
            GameObject tempListing = Instantiate(roomListingPrefab, roomsPanel);
            RoomListItem tempButton = tempListing.GetComponent<RoomListItem>();
            tempButton.roomName = room.Name;
            tempButton.roomSize = room.MaxPlayers;
            tempButton.SetRoom();
        }
    }

    public void CreateRoom() {
        RoomOptions roomOptions = new RoomOptions() {
            IsVisible = true, IsOpen = true, MaxPlayers = (byte) roomSize
        };
        PhotonNetwork.CreateRoom(roomName, roomOptions);
        SceneManager.LoadScene(1);
    }

    public override void OnCreateRoomFailed(short returnCode, string message) {
        Debug.Log("Trued to create a new room but failed, there must be a room with the same name!");
    }

    public void OnRoomNameChanged(string nameIn) {
        roomName = nameIn;
    }

    public void OnRoomSizeChanged(string sizeIn) {
        roomName = sizeIn;
    }

    public void JoinLobbyOnClick() {
        if (!PhotonNetwork.InLobby) {
            PhotonNetwork.JoinLobby();
        }
    }
}