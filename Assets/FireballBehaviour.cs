﻿using System;
using System.Linq;
using Photon.Pun;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class FireballBehaviour : MonoBehaviour {
    private float speed;
    private float damage;

    private float timer = 3f;

    [SerializeField] private PhotonView photonView;

    private void Start() {
        photonView = GetComponent<PhotonView>();
    }

    void FixedUpdate() {
        MoveProjectileForward();

        if (!PhotonNetwork.IsMasterClient) return;
        timer -= Time.deltaTime;
        // photonView.RPC("SyncPosition", RpcTarget.Others, transform.position);

        if (IsEnemyNearby()) {
            DealDamage();
            photonView.RPC("GlobalDestroy", RpcTarget.All);
        }

        if (IsDurationOver()) photonView.RPC("GlobalDestroy", RpcTarget.All);
    }

    public void InitAbilityObject(AbilityData data) {
            GetComponent<PhotonView>().RPC("SyncronizeInitData", RpcTarget.All, data.speed, data.damage);
    }

    [PunRPC]
    private void SyncronizeInitData(float aSpeed, float aDamage) {
        speed = aSpeed;
        damage = aDamage;
    }

    [PunRPC]
    private void GlobalDestroy() {
        if (GetComponent<PhotonView>().IsMine)
            PhotonNetwork.Destroy(gameObject);
    }

    private void MoveProjectileForward() {
        gameObject.GetComponent<Rigidbody>().MovePosition((Vector3) gameObject.transform.position +
                                                          (gameObject.transform.forward * (Time.deltaTime * speed)));
    }

    [PunRPC]
    private void SyncPosition(Vector3 newPos) {
        transform.position = newPos;
    }

    private bool IsEnemyNearby() {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 0.5f);
        return hitColliders.Length > 0 && hasEnemyCollider(hitColliders);
    }

    private bool hasEnemyCollider(Collider[] colliders) {
        foreach (var coll in colliders) {
            if (coll.gameObject.CompareTag("Enemy")) return true;
        }

        return false;
    }

    private bool IsDurationOver() {
        return timer <= 0;
    }

    private void DealDamage() {
        GameObject playerUnit = Constants.LocalPlayerObject.GetComponent<PhotonNetworkPlayer>().GetMyPlayerUnit()
            .gameObject;
        getOneTargetEnemy().TakeDamage(playerUnit, (int) damage);
    }

    private Unit getOneTargetEnemy() {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 0.5f);
        foreach (var collider in hitColliders) {
            if (collider.gameObject.CompareTag("Enemy")) return collider.gameObject.GetComponent<Unit>();
        }

        return null;
    }
}