﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class LoadingLevel : MonoBehaviourPunCallbacks {
    [SerializeField] private GameObject loadingScreen;
    private PhotonView photonView;

    private void Awake() {
        photonView = GetComponent<PhotonView>();
    }

    void Start() {
        loadingScreen.gameObject.SetActive(true);

        if (!PhotonNetwork.IsMasterClient) return;
        StartCoroutine(CheckIsEveryoneFinishedLoading());
    }

    private IEnumerator CheckIsEveryoneFinishedLoading() {
        bool isEveryoneFinishedLoading = false;
        Debug.Log("PlayerCount: " + PhotonNetwork.CurrentRoom.PlayerCount);
        while (!isEveryoneFinishedLoading) {
            isEveryoneFinishedLoading = true;
            foreach (var currentRoomPlayer in PhotonNetwork.CurrentRoom.Players) {
                Hashtable properties = currentRoomPlayer.Value.CustomProperties;
                if (!(bool) properties[Constants.PlayerProperty.LoadingFinished]) {
                    isEveryoneFinishedLoading = false;
                }
            }

            yield return new WaitForSeconds(1f);
        }

        Hashtable roomProperties = PhotonNetwork.CurrentRoom.CustomProperties;
        roomProperties.Add(Constants.RoomProperty.AllClientLoadingFinished, true);
        PhotonNetwork.CurrentRoom.SetCustomProperties(roomProperties);
        photonView.RPC("EnterGame", RpcTarget.All);
    }

    [PunRPC]
    private void EnterGame() {
        loadingScreen.SetActive(false);
    }
}