﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class CharacterSelector : MonoBehaviour {
    [SerializeField] private Text characterType;
    [SerializeField] public List<GameObject> characterPrefabs;
    private List<GameObject> InstantiatedCharacters = new List<GameObject>();
    [SerializeField] private List<GameObject> CharacterPrefabsToSpawn = new List<GameObject>();
    public int selectedCharacterIndex = 0;
    private GameObject selectedCharacter;
    public static bool rotatingLeft = false, rotatingRight = false;
    private float rotation = 0, speed = 3;
    private bool IsLockedSelection = false;
    private PhotonNetworkPlayer _photonNetworkPlayer;

    private Color HighlightedGreen = new Color(190, 255, 190),
        White = new Color(255, 255, 255),
        Transparent = new Color(255, 255, 255, 130);


    void Start() {
        PlaceCharacters();
        selectedCharacter = CharacterPrefabsToSpawn[selectedCharacterIndex];
        characterType.text = selectedCharacter.GetComponent<Unit>().classType;
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            GetPrevCharacter();
        }

        if (Input.GetKeyDown(KeyCode.RightArrow)) {
            GetNextCharacter();
        }

        if (rotatingLeft) {
            if (rotation > 0) {
                transform.Rotate(0, -speed, 0);
                rotation -= speed;
            }
            else {
                rotation = 0;
                rotatingLeft = false;
            }
        }

        if (rotatingRight) {
            if (rotation < 0) {
                transform.Rotate(0, speed, 0);
                rotation += speed;
            }
            else {
                rotation = 0;
                rotatingRight = false;
            }
        }
    }

    public void GetNextCharacter() {
        if (rotation != 0 || IsLockedSelection) return;
        rotation -= 360 / InstantiatedCharacters.Count;
        rotatingRight = true;

        selectedCharacterIndex += 1;
        if (selectedCharacterIndex == characterPrefabs.Count) selectedCharacterIndex = 0;
        selectedCharacter = CharacterPrefabsToSpawn[selectedCharacterIndex];
        characterType.text = selectedCharacter.GetComponent<Unit>().classType;
    }

    public void GetPrevCharacter() {
        if (rotation != 0 || IsLockedSelection) return;
        rotation += 360 / InstantiatedCharacters.Count;
        rotatingLeft = true;

        selectedCharacterIndex -= 1;
        if (selectedCharacterIndex == -1) selectedCharacterIndex = characterPrefabs.Count - 1;
        selectedCharacter = CharacterPrefabsToSpawn[selectedCharacterIndex];
        characterType.text = selectedCharacter.GetComponent<Unit>().classType;
    }

    public void SetButtonColor(Button _button) {
        if (!IsLockedSelection) {
            ColorBlock colorVar = _button.colors;
            colorVar.highlightedColor = White;
            colorVar.normalColor = Transparent;
            _button.colors = colorVar;
        }
        else {
            ColorBlock colorVar = _button.colors;
            colorVar.highlightedColor = HighlightedGreen;
            colorVar.normalColor = White;
            _button.colors = colorVar;
        }
    }

    public void SetButtonText(Text _text) {
        if (!IsLockedSelection) _text.text = "Select Hero";
        else _text.text = "Cancel";
    }

    public void ToggleSelection() {
        if (!IsLockedSelection) {
            SelectCharacter();
        }
        else {
            UnselectCharacter();
        }
    }

    private void UnselectCharacter() {
        IsLockedSelection = false;
        GameObject.Find("LocalPlayerObject").GetComponent<PhotonNetworkPlayer>().SetSelectedCharacter(null);
    }

    private void SelectCharacter() {
        IsLockedSelection = true;
        GameObject.Find("LocalPlayerObject").GetComponent<PhotonNetworkPlayer>()
            .SetSelectedCharacter(selectedCharacter);
    }

    private void PlaceCharacters() {
        float radius = 3;
        for (int i = 0; i < characterPrefabs.Count; i++) {
            float angle = i * Mathf.PI * 2f / characterPrefabs.Count;
            Vector3 newPos = new Vector3(Mathf.Cos(angle) * radius, 0.1f, Mathf.Sin(angle) * radius);
            Vector3 direction = transform.position - newPos;
            direction.y = 0;
            Quaternion rotation = Quaternion.LookRotation(direction);
            GameObject go = Instantiate(characterPrefabs[i], newPos, rotation);
            go.transform.Rotate(0, -90, 0);
            go.transform.SetParent(transform);
            InstantiatedCharacters.Add(go);
        }

        transform.Rotate(0, 120, 0);
    }
}