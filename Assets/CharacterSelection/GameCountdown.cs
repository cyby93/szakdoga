﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class GameCountdown : MonoBehaviourPunCallbacks {
    [SerializeField] private Text CountdownNumber;
    [SerializeField] private GameObject LoadingLayer;
    private int number = 5;
    private float timer = 1;
    public int fontSize = 50;

    [SerializeField] private GameObject CountdownLayer;
    private IEnumerator countdown;

    private void Start() {
        countdown = Countdown();
    }

    void Update() {
        CountdownNumber.fontSize = fontSize;
        if (Input.GetKeyDown(KeyCode.C)) {
            StartCoroutine(countdown);
        }

        if (Input.GetKeyDown(KeyCode.V)) {
            StopCountdown();
        }
    }

    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged) {
        base.OnRoomPropertiesUpdate(propertiesThatChanged);
        if (propertiesThatChanged.ContainsKey(Constants.RoomProperty.IsCountdownOn)) {
            StartCoroutine(countdown);
        }
    }

    private IEnumerator Countdown() {
        int timeUntilStart = number;
        CountdownLayer.SetActive(true);
        while (timeUntilStart > 0) {
            CountdownNumber.text = timeUntilStart.ToString();
            yield return new WaitForSeconds(1f);
            timeUntilStart--;
        }
        LoadingLayer.transform.GetChild(0).GetComponent<Text>().text = "Loading game...";
        LoadingLayer.SetActive(true);

        CountdownLayer.SetActive(false);
        StartGame();
    }

    private void StopCountdown() {
        StopCoroutine(countdown);
        CountdownLayer.SetActive(false);
    }

    public void StartGame() {
        if (!PhotonNetwork.IsMasterClient) return;
        Hashtable properties = PhotonNetwork.CurrentRoom.CustomProperties;
        properties[Constants.RoomProperty.GameStarted] = true;
        PhotonNetwork.CurrentRoom.SetCustomProperties(properties);
    }
}