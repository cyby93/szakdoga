﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPartyStatusConfig : MonoBehaviour {
    [SerializeField] private Text nickNameText;
    [SerializeField] private Image StatusColor;

    public void Setup(Player player) {
        nickNameText.text = player.NickName;
        if (player.CustomProperties["IsReady"] != null && player.CustomProperties["IsReady"].Equals(true))
            StatusColor.color = new Color(0, 150, 0);
        else
            StatusColor.color = new Color(150, 0, 0);
    }
}