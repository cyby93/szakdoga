﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class StructureCanvas : MonoBehaviour {
    private GameObject structureGameObject;
    private UnitStats stats;
    [SerializeField] private Image healthbar;
    private int maxHealth;


    private void Awake() {
        structureGameObject = gameObject.transform.parent.gameObject;
        stats = structureGameObject.GetComponent<UnitStats>();
    }

    void Update() {
        if (transform.rotation != Camera.main.transform.rotation) transform.rotation = Camera.main.transform.rotation;
        if (gameObject.transform.parent.GetComponent<UnitStats>().GetHealth() <= 0) {
            DestroyImmediate(gameObject.transform.parent.gameObject);
        }

        UpdateHealth();
        if(Input.GetKeyDown(KeyCode.A))
            Debug.Log(stats.GetHealth());
    }

    private void UpdateHealth() {
        healthbar.fillAmount = (stats.GetHealth() / (float) stats.maxHealth);
    }
}